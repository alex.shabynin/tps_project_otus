// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Controllers/SAITurretController.h"

#include "SAITurret.h"
//#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Sight.h"



void ASAITurretController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if(IsValid(InPawn))
	{
		checkf(InPawn->IsA<ASAITurret>(),TEXT("ASAITurretController::SetPawn can possess only turret pawns"));
		CachedTurretPawn = StaticCast<ASAITurret*>(InPawn);
	}
	else
	{
		CachedTurretPawn = nullptr;
	}
}

void ASAITurretController::ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors)
{
	Super::ActorsPerceptionUpdated(UpdatedActors);
	if(!CachedTurretPawn.IsValid())
	{
		return;
	}

	AActor* ClosestActor = GetClosestSensedActor(UAISense_Sight::StaticClass());
	
	CachedTurretPawn->SetCurrentTarget(ClosestActor);

	// То что ниже - эксперимент, возможно убрать в будущем
	if(IsValid(ClosestActor))
	{
		SetFocus(ClosestActor);
	}
}

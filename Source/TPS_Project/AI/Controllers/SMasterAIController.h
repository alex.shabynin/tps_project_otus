// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SMasterAIController.generated.h"


class UAISense;

UCLASS()
class TPS_PROJECT_API ASMasterAIController : public AAIController
{
	GENERATED_BODY()

public:
	ASMasterAIController();

protected:
	AActor* GetClosestSensedActor(TSubclassOf<UAISense> SenseClass) const;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/Controllers/SMasterAIController.h"
#include "SEnemyCharacter.h"
#include "SCharacterAIController.generated.h"

/**
 * 
 */
UCLASS()
class TPS_PROJECT_API ASCharacterAIController : public ASMasterAIController
{
	GENERATED_BODY()

public:
	virtual void SetPawn(APawn* InPawn) override;

	virtual void ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors) override;

	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
	void SetupPatrolling();

protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="Movement")
	float TargetReachRadius = 100.0f;
	
	//virtual void BeginPlay() override;


private:
	
	void TryMoveToNextTarget();

	bool IsTargetReached(FVector TargetLocation) const;

	TWeakObjectPtr<ASEnemyCharacter> CachedAICharacter;

	bool bIsPatrolling = false;
};

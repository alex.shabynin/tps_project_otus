// Fill out your copyright notice in the Description page of Project Settings. 


#include "SEnemyAIController.h"

#include "SEnemyCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

void ASEnemyAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	APawn* PawnPlayer = UGameplayStatics::GetPlayerPawn(GetWorld(),0);

	if(AIBehaviorTree == nullptr) return;

	if(TargetActor != nullptr)
	{
		if(LineOfSightTo(PawnPlayer))
		{
			GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"),PawnPlayer->GetActorLocation());
			GetBlackboardComponent()->SetValueAsVector(TEXT("LastKnownPlayerLocation"),PawnPlayer->GetActorLocation());

		}
		else
		{
			GetBlackboardComponent()->ClearValue(TEXT("PlayerLocation"));
		}
	}
	else
	{
		for(const auto& it: EnemyTeam)
		{
			if(it != nullptr && LineOfSightTo(it))
			{
				TargetActor = it;
			}
		}
	}
	/*SetFocus(PawnPlayer);
	MoveToActor(PawnPlayer,50.0f);*/
}

void ASEnemyAIController::BeginPlay()
{
	Super::BeginPlay();

	EnemyTeam.Empty();
	TargetActor = nullptr;

	if(AIBehaviorTree !=nullptr)
	{
		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(),ASEnemyCharacter::StaticClass(),FoundActors);
		ASEnemyCharacter* ThisPawnEnemy = Cast<ASEnemyCharacter>(this->GetCharacter());

		for (const auto& PossibleEnemy : FoundActors)
		{
			ASEnemyCharacter* EnemyPawn = Cast<ASEnemyCharacter>(PossibleEnemy);
			if(EnemyPawn != nullptr && ThisPawnEnemy->bIsPlayerTeam != EnemyPawn->bIsPlayerTeam)
			{
				EnemyTeam.Push(EnemyPawn);
			}
		}
		if(!ThisPawnEnemy->bIsPlayerTeam)
		{
			APawn* PawnPlayer = UGameplayStatics::GetPlayerPawn(GetWorld(),0);
			EnemyTeam.Push(PawnPlayer);
		}
		
		APawn* PawnPlayer = UGameplayStatics::GetPlayerPawn(GetWorld(),0);
		RunBehaviorTree(AIBehaviorTree);
	
		GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"),GetPawn()->GetActorLocation());
		
	}
}

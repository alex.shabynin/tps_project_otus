// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Controllers/SCharacterAIController.h"

#include "AIPatrollingComponent.h"
#include "SBaseCharacterMovementComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AISense.h"
#include "Perception/AISense_Sight.h"

void ASCharacterAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if(IsValid(InPawn))
	{
		checkf(InPawn->IsA<ASEnemyCharacter>(),TEXT("ASCharacterAIController::SetPawn ASCharacterAIController can possess only ASEnemyCharacter"));
		CachedAICharacter = StaticCast<ASEnemyCharacter*>(InPawn);
		RunBehaviorTree(CachedAICharacter->GetBehaviorTree());
		SetupPatrolling();
	}
	else
	{
		CachedAICharacter = nullptr;
	}
}

void ASCharacterAIController::ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors)
{
	Super::ActorsPerceptionUpdated(UpdatedActors);
	if(!CachedAICharacter.IsValid())
	{
		return;
	}
	TryMoveToNextTarget();
	
}

void ASCharacterAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);
	if(!Result.IsSuccess())
	{
		return;
	}
	TryMoveToNextTarget();
}

void ASCharacterAIController::SetupPatrolling()
{
	UAIPatrollingComponent* PatrollingComponent = CachedAICharacter->GetPatrollingComponent();
	if(PatrollingComponent->CanPatrol())
	{
		FVector ClosestWaypont = PatrollingComponent->SelectClosestWayPoint();
		if(IsValid(Blackboard))
		{
			Blackboard->SetValueAsVector(BB_NextLocation,ClosestWaypont);
			Blackboard->SetValueAsObject(BB_CurrentTarget,nullptr);
		}
		bIsPatrolling = true;
	}
}



void ASCharacterAIController::TryMoveToNextTarget()
{
	UAIPatrollingComponent* PatrollingComponent = CachedAICharacter->GetPatrollingComponent();
	USBaseCharacterMovementComponent* MovementComponent = CachedAICharacter->GetSBaseCharacterMovementComponent();
	AActor* ClosestActor = GetClosestSensedActor(UAISense_Sight::StaticClass());
	
	if(IsValid(ClosestActor))
	{
		if(IsValid(Blackboard))
		{
			
			Blackboard->SetValueAsObject(BB_CurrentTarget,ClosestActor);
			SetFocus(ClosestActor,EAIFocusPriority::Gameplay);
			MovementComponent->MaxWalkSpeed = 450.0f;
		}
		bIsPatrolling = false;
	}
	else if(PatrollingComponent->CanPatrol())
	{
		FVector Waypoint = bIsPatrolling ? PatrollingComponent->SelectNextWayPoint() : PatrollingComponent->SelectClosestWayPoint();
		if(IsValid(Blackboard))
		{
			ClearFocus(EAIFocusPriority::Gameplay);
			Blackboard->SetValueAsVector(BB_NextLocation,Waypoint);
			Blackboard->SetValueAsObject(BB_CurrentTarget,nullptr);
		}
		bIsPatrolling = true;
	}
}

bool ASCharacterAIController::IsTargetReached(FVector TargetLocation) const
{
	return (TargetLocation - CachedAICharacter->GetActorLocation()).SizeSquared() <= FMath::Square(TargetReachRadius);
}

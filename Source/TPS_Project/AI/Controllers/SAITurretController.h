// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "AIController.h"
#include "SMasterAIController.h"
#include "SAITurretController.generated.h"

class ASAITurret;
/**
 * 
 */
UCLASS()
class TPS_PROJECT_API ASAITurretController : public ASMasterAIController
{
	GENERATED_BODY()

public:
	//ASAITurretController();

	virtual void SetPawn(APawn* InPawn) override;

	virtual void ActorsPerceptionUpdated(const TArray<AActor*>& UpdatedActors) override;

private:
	TWeakObjectPtr<ASAITurret> CachedTurretPawn;
	
};

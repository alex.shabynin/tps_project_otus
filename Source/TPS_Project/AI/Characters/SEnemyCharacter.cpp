// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/SEnemyCharacter.h"

#include "AIPatrollingComponent.h"
#include "HealthBarComponent.h"
#include "SCharacterAttributesComponent.h"
#include "SEnemyAIController.h"


ASEnemyCharacter::ASEnemyCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	//AIControllerClass = ASEnemyAIController;

	HealthBarWidget = CreateDefaultSubobject<UHealthBarComponent>(TEXT("HealthBar"));
	HealthBarWidget->SetupAttachment(RootComponent);
	AIPatrollingComponent = CreateDefaultSubobject<UAIPatrollingComponent>(TEXT("AIPatrollingComponent"));
	
}


void ASEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(HealthBarWidget)
	{
		HealthBarWidget->SetHealthPercent(CharacterAttributesComponent->GetHealthPercent());
	}
	float CurrentHealth = CharacterAttributesComponent->GetHealth();
	if(CurrentHealth <= 0)
	{
		HealthBarWidget->SetVisibility(false);
	}
}

UAIPatrollingComponent* ASEnemyCharacter::GetPatrollingComponent() const
{
	return AIPatrollingComponent;
}

USBaseCharacterMovementComponent* ASEnemyCharacter::GetSBaseCharacterMovementComponent()
{
	return SBaseCharacterMovementComponent;
}

UBehaviorTree* ASEnemyCharacter::GetBehaviorTree() const
{
	return BehaviorTree;
}

const USCharacterAttributesComponent* ASEnemyCharacter::GetCharacterAttributesComponent() const
{
	return CharacterAttributesComponent;
}






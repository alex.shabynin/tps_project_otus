// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Characters/SBaseCharacter.h"
#include "SEnemyCharacter.generated.h"

class UBehaviorTree;
class UAIPatrollingComponent;
class UHealthBarComponent;
class USCharacterAttributesComponent;
class USBaseCharacterMovementComponent;
/**
 * 
 */
UCLASS(Blueprintable)
class TPS_PROJECT_API ASEnemyCharacter : public ASBaseCharacter
{
	GENERATED_BODY()

public:
	ASEnemyCharacter(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
	bool bIsPlayerTeam;

	virtual void Tick(float DeltaTime) override;

	UAIPatrollingComponent* GetPatrollingComponent() const;
	USBaseCharacterMovementComponent* GetSBaseCharacterMovementComponent();

	UBehaviorTree* GetBehaviorTree() const;
	
protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Widgets")
	UHealthBarComponent* HealthBarWidget;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="Components")
	UAIPatrollingComponent* AIPatrollingComponent;
	
	const USCharacterAttributesComponent* GetCharacterAttributesComponent() const;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AI")
	UBehaviorTree* BehaviorTree;
};

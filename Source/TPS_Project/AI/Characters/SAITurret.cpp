// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Characters/SAITurret.h"

#include "AIController.h"
#include "SWeaponBarrelComponent.h"

ASAITurret::ASAITurret()
{
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* TurretRoot = CreateDefaultSubobject<USceneComponent>(TEXT("TurretRoot"));
	SetRootComponent(TurretRoot);

	TurretBaseComponent = CreateDefaultSubobject<USceneComponent>(TEXT("TurretBase"));
	TurretBaseComponent->SetupAttachment(TurretRoot);

	TurretBarrelComponent = CreateDefaultSubobject<USceneComponent>(TEXT("TurretBarrel"));
	TurretBarrelComponent->SetupAttachment(TurretBaseComponent);

	WeaponBarrelComponent = CreateDefaultSubobject<USWeaponBarrelComponent>(TEXT("WeaponBarrel"));
	WeaponBarrelComponent->SetupAttachment(TurretBarrelComponent);
	
}



void ASAITurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	switch (CurrentTurretState)
	{
	case ETurretState::Searching:
		{
			SearchingMovement(DeltaTime);
			break;
		}
	case ETurretState::Firing:
		{
			FiringMovement(DeltaTime);
			break;
		}
	}

}

FVector ASAITurret::GetPawnViewLocation() const
{
	return WeaponBarrelComponent->GetComponentLocation();
}

FRotator ASAITurret::GetViewRotation() const
{
	//return Super::GetViewRotation();
	return WeaponBarrelComponent->GetComponentRotation();
}

void ASAITurret::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	AAIController* AIController = Cast<AAIController>(NewController);
	if(IsValid(AIController))
	{
		FGenericTeamId TeamId((uint8)Team);
		AIController->SetGenericTeamId(TeamId);
	}
}

void ASAITurret::SetCurrentTarget(AActor* NewTarget)
{
	CurrentTarget = NewTarget;
	ETurretState NewState = IsValid(CurrentTarget) ? ETurretState::Firing : ETurretState::Searching;
	SetCurrentTurretState(NewState);
}

void ASAITurret::SearchingMovement(float DeltaTime)
{
	FRotator TurretBaseRotation = TurretBaseComponent->GetRelativeRotation();
	TurretBaseRotation.Yaw += DeltaTime * BaseSearchingRotationRate;
	TurretBaseComponent->SetRelativeRotation(TurretBaseRotation);

	FRotator TurretBarrelRotation = TurretBarrelComponent->GetRelativeRotation();
	TurretBarrelRotation.Pitch = FMath::FInterpTo(TurretBarrelRotation.Pitch,0.0f,DeltaTime,BarrelPitchRotationRate);
	TurretBarrelComponent->SetRelativeRotation(TurretBarrelRotation);
}

void ASAITurret::FiringMovement(float DeltaTime)
{
	FVector BaseLookAtDirection = (CurrentTarget->GetActorLocation() - TurretBaseComponent->GetComponentLocation()).GetSafeNormal2D();
	FQuat LookAtQuat = BaseLookAtDirection.ToOrientationQuat();
	FQuat TargetQuat = FMath::QInterpTo(TurretBaseComponent->GetComponentQuat(),LookAtQuat,DeltaTime,BaseFiringInterpSpeed);
	TurretBaseComponent->SetWorldRotation(TargetQuat);

	FVector BarrelLookAtDirection = (CurrentTarget->GetActorLocation() - TurretBarrelComponent->GetComponentLocation()).GetSafeNormal();
	float LookAtPitchAngle = BarrelLookAtDirection.ToOrientationRotator().Pitch;
	FRotator BarrelLocalRotation = TurretBaseComponent->GetRelativeRotation();
	BarrelLocalRotation.Pitch = FMath::FInterpTo(BarrelLocalRotation.Pitch,LookAtPitchAngle,DeltaTime,BarrelPitchRotationRate);
	TurretBarrelComponent->SetRelativeRotation(BarrelLocalRotation);
}

void ASAITurret::SetCurrentTurretState(ETurretState NewState)
{
	bool bStateChanged = NewState != CurrentTurretState;
	CurrentTurretState = NewState;
	if(!bStateChanged)
	{
		return;
	}
	switch(CurrentTurretState)
	{
	case ETurretState::Searching:
		{
			GetWorld()->GetTimerManager().ClearTimer(ShotTimer);
			break;
		}
	case ETurretState::Firing:
		{
			GetWorld()->GetTimerManager().SetTimer(ShotTimer,this, &ASAITurret::MakeShot,GetFireInterval(),true,FireDelayTime);
			break;
		}
	}
}

float ASAITurret::GetFireInterval() const
{
	return 60.0f / RateOfFire;
}

void ASAITurret::MakeShot()
{
	FVector ShotLocation = WeaponBarrelComponent->GetComponentLocation();
	FVector ShotDirection = WeaponBarrelComponent->GetComponentRotation().RotateVector(FVector::ForwardVector);
	float SpreadAngle = FMath::DegreesToRadians(BulletSpreadAngle);
	WeaponBarrelComponent->Shot(ShotLocation,ShotDirection,SpreadAngle);
}





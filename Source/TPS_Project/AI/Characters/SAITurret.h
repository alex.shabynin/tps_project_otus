// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "SAITurret.h"
#include "TPSTypes.h"
#include "GameFramework/Pawn.h"
#include "SAITurret.generated.h"


enum class ETeams : uint8;

UENUM(BlueprintType)
enum class ETurretState : uint8
{
	Searching,
	Firing
};

class USWeaponBarrelComponent;

UCLASS()
class TPS_PROJECT_API ASAITurret : public APawn
{
	GENERATED_BODY()

public:
	ASAITurret();

	void SetCurrentTarget(AActor* NewTarget);
	
	virtual void Tick(float DeltaTime) override;

	virtual FVector GetPawnViewLocation() const override;
	virtual FRotator GetViewRotation() const override;
	virtual void PossessedBy(AController* NewController) override;

protected:
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="Components")
	USceneComponent* TurretBaseComponent;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="Components")
	USceneComponent* TurretBarrelComponent;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="Components")
	USWeaponBarrelComponent* WeaponBarrelComponent;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Turret parameters", meta=(ClampMin=0.0f,UIMin=0.0f))
	float BaseSearchingRotationRate = 60.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Turret parameters", meta=(ClampMin=0.0f,UIMin=0.0f))
	float BaseFiringInterpSpeed = 5.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Turret parameters", meta=(ClampMin=0.0f,UIMin=0.0f))
	float BarrelPitchRotationRate = 60.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Turret parameters", meta=(ClampMin=0.0f,UIMin=0.0f))
	float MaxBarrelPitchAngle = 45.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Turret parameters", meta=(ClampMin=-90.0f,UIMin=-90.0f))
	float MinBarrelPitchAngle = -30.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Turret parameters | Fire", meta=(ClampMin=1.0f,UIMin=1.0f))
	float RateOfFire = 300.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Turret parameters", meta=(ClampMin=0.0f,UIMin=0.0f))
	float BulletSpreadAngle = 1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Turret parameters", meta=(ClampMin=0.0f,UIMin=0.0f))
	float FireDelayTime = 2.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Turret parameters | Team")
	ETeams Team = ETeams::Enemy;
	
private:
	void SearchingMovement(float DeltaTime);
	void FiringMovement(float DeltaTime);
	void SetCurrentTurretState(ETurretState NewState);
	void MakeShot();
	ETurretState CurrentTurretState = ETurretState::Searching;

	AActor* CurrentTarget = nullptr;

	float GetFireInterval() const;

	FTimerHandle ShotTimer;

};

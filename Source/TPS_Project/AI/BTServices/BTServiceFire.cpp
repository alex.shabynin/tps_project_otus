// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/BTServices/BTServiceFire.h"

#include "AIController.h"
#include "SBaseCharacter.h"
#include "SRangeWeaponItem.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTServiceFire::UBTServiceFire()
{
	NodeName = "Fire";
}

void UBTServiceFire::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	AAIController* AIController = OwnerComp.GetAIOwner();
	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	
	
	if(!IsValid(AIController) || !IsValid(Blackboard))
	{
		return;
	}
	ASBaseCharacter* Character = Cast<ASBaseCharacter>(AIController->GetPawn());


	
	if(!IsValid(Character))
	{
		return;
	}
	const UCharacterEquipmentComponent* EquipmentComponent = Character->GetCharacterEquipmentComponent();
	ASRangeWeaponItem* RangeWeaponItem = EquipmentComponent->GetCurrentRangeWeapon();
	if(!IsValid(RangeWeaponItem))
	{
		return;
	}
	AActor* CurrentTarget = Cast<AActor>(Blackboard->GetValueAsObject(TargetKey.SelectedKeyName));
	if(!IsValid(CurrentTarget))
	{
		Character->StopFire();
		return;
	}
	float DistSq = FVector::DistSquared(CurrentTarget->GetActorLocation(),Character->GetActorLocation());
	if(DistSq > FMath::Square(MaxFireDistance))
	{
		Character->StopFire();
		return;
	}
	if(!(RangeWeaponItem->IsReloading() || RangeWeaponItem->IsFiring()))
	{
		Character->StartFire();
	}
	
	

}

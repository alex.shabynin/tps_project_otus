// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "HealthBarComponent.generated.h"

class USAIHealthBar;
/**
 * 
 */
UCLASS()
class TPS_PROJECT_API UHealthBarComponent : public UWidgetComponent
{
	GENERATED_BODY()

public:
	void SetHealthPercent(float Percent);

protected:
	UPROPERTY()
	USAIHealthBar* HealthBarWidget;
	
};

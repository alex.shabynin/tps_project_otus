// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/HealthBarComponent.h"

#include "SAIHealthBar.h"
#include "Components/ProgressBar.h"

void UHealthBarComponent::SetHealthPercent(float Percent)
{
	if(HealthBarWidget == nullptr)
	{
		HealthBarWidget = Cast<USAIHealthBar>(GetUserWidgetObject());

	}
	if(HealthBarWidget && HealthBarWidget->HealthBar)
	{
		HealthBarWidget->HealthBar->SetPercent(Percent);
	}
}

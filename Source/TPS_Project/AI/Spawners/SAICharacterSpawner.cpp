// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Spawners/SAICharacterSpawner.h"

#include "SEnemyCharacter.h"

ASAICharacterSpawner::ASAICharacterSpawner()
{
	USceneComponent* SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SpawnRoot"));
	SetRootComponent(SceneRoot);
  
}


void ASAICharacterSpawner::SpawnAI()
{
	if(!bCanSpawn || !IsValid(CharacterClass))
	{
		return;
	}

	ACharacter* AnyAICharacter = GetWorld()->SpawnActor<ACharacter>(CharacterClass, GetTransform());
	if(!IsValid(AnyAICharacter->Controller))
	{
		AnyAICharacter->SpawnDefaultController();
	}

	/*ASEnemyCharacter* AICharacter = GetWorld()->SpawnActor<ASEnemyCharacter>(CharacterClass, GetTransform()); //Remove limitation of Character class (SEnemyCharacter)
	if(!IsValid(AICharacter->Controller))
	{
		AICharacter->SpawnDefaultController();
	}*/

	if(bDoOnce)
	{
		UnSubscribeFromTrigger();
		bCanSpawn = false;
	}
}

void ASAICharacterSpawner::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	if (PropertyChangedEvent.Property->GetName() == GET_MEMBER_NAME_STRING_CHECKED(ASAICharacterSpawner,SpawnTriggerActor))
	{
		SpawnTrigger = SpawnTriggerActor;
		if(SpawnTrigger.GetInterface())
		{
			if(!SpawnTrigger->HasOnInteractionCallback())
			{
				SpawnTriggerActor = nullptr;
				SpawnTrigger = nullptr;
			}
		}
		else
		{
			SpawnTriggerActor = nullptr;
			SpawnTrigger = nullptr;
		}
	}
}

void ASAICharacterSpawner::BeginPlay()
{
	Super::BeginPlay();

	if(SpawnTrigger.GetInterface())
	{
		TriggerHandle = SpawnTrigger->AddOnInteractionUFunction(this, FName("SpawnAI"));
	}
	if(bIsSpawnOnStart)
	{
		SpawnAI();
	}
	
}

void ASAICharacterSpawner::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	UnSubscribeFromTrigger();
	Super::EndPlay(EndPlayReason);
}

void ASAICharacterSpawner::UnSubscribeFromTrigger()
{
	if(TriggerHandle.IsValid() && SpawnTrigger.GetInterface())
	{
		SpawnTrigger->RemoveOnInteractionDelegate(TriggerHandle);
	}
}




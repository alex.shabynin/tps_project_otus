// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactive.h"
#include "GameFramework/Actor.h"
#include "SAICharacterSpawner.generated.h"

class ASEnemyCharacter;

UCLASS()
class TPS_PROJECT_API ASAICharacterSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	ASAICharacterSpawner();

	UFUNCTION()
	void SpawnAI();

protected:

	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="AI Spawner")
	//TSubclassOf<ASEnemyCharacter> CharacterClass; //Remove limitation of Character class (SEnemyCharacter)
	TSubclassOf<ACharacter> CharacterClass;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="AI Spawner")
	bool bIsSpawnOnStart;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="AI Spawner")
	bool bDoOnce = false;

	//Actor implementing IInteractable interface
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="AI Spawner")
	AActor* SpawnTriggerActor;
	

private:
	bool bCanSpawn = true;

	UPROPERTY()
	TScriptInterface<IInteractable> SpawnTrigger;

	FDelegateHandle TriggerHandle;

	void UnSubscribeFromTrigger();

};

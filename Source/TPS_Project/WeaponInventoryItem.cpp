// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponInventoryItem.h"
#include "SEquippableItem.h"

UWeaponInventoryItem::UWeaponInventoryItem()
{
	bIsConsumable = true;
}

void UWeaponInventoryItem::SetEquipWeaponClass(TSubclassOf<ASEquippableItem>& WeaponClass)
{
	EquipWeaponClass = WeaponClass;
}

TSubclassOf<ASEquippableItem> UWeaponInventoryItem::GetEquipWeaponClass() const
{
	return EquipWeaponClass;
}

// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TPS_Project : ModuleRules
{
	public TPS_Project(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore","Niagara", "UMG","GameplayTags","NavigationSystem","AIModule","GameplayTasks" });

		PrivateDependencyModuleNames.AddRange(new string[] { "AnimGraphRuntime", "Slate", "SlateCore" });
		
		PrivateIncludePaths.AddRange(new string[]{Name});
		
		PublicIncludePaths.AddRange(new string[]
			{
				"TPS_Project/Actors",
				"TPS_Project/Actors/Equipment",
				"TPS_Project/Actors/Actions",
				"TPS_Project/Characters",
				"TPS_Project/Characters/Animation",
				"TPS_Project/Components",
				"TPS_Project/Subsystems",
				"TPS_Project/UI",
				"TPS_Project/Utils",
				"TPS_Project/AI",
				"TPS_Project/AI/Characters",
				"TPS_Project/AI/Controllers",
				"TPS_Project/AI/BTTasks",
				"TPS_Project/AI/BTServices",
				"TPS_Project/AI/Spawners"
			}
		);

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Actions/SActionEffects.h"

USActionEffects::USActionEffects()
{
	bAutoActivate = true;
}

void USActionEffects::StartAction_Implementation(AActor* Instigator)
{
	Super::StartAction_Implementation(Instigator);
	if(Duration > 0.0f)
	{
		FTimerDelegate Delegate;
		Delegate.BindUObject(this, &USActionEffects::StopAction,Instigator);

		GetWorld()->GetTimerManager().SetTimer(DurationHandle,Delegate,Duration,false);
	}

	if (Period > 0.0f)
	{
		FTimerDelegate Delegate;
		Delegate.BindUObject(this, &USActionEffects::ExecutePeriodEffect,Instigator);
		GetWorld()->GetTimerManager().SetTimer(PeriodHandle,Delegate,Period,true);

	}
}

void USActionEffects::StopAction_Implementation(AActor* Instigator)
{
	
	if(GetWorld()->GetTimerManager().GetTimerRemaining(PeriodHandle) < KINDA_SMALL_NUMBER)
	{
		ExecutePeriodEffect(Instigator);
	}
	Super::StopAction_Implementation(Instigator);
	GetWorld()->GetTimerManager().ClearTimer(PeriodHandle);
	GetWorld()->GetTimerManager().ClearTimer(DurationHandle);

	USActionComponent* Comp = GetOwningComponent();
	if(Comp)
	{
		Comp->RemoveAction(this);
	}

}

float USActionEffects::GetTimeRemaining() const
{
	float EndTime = TimeStarted + Duration;
	
	return EndTime - GetWorld()->TimeSeconds;
}

void USActionEffects::ExecutePeriodEffect_Implementation(AActor* Instigator)
{
	
}


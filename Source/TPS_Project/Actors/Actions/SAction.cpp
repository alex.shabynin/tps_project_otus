// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Actions/SAction.h"

// Sets default values
/*USAction::USAction()
{
 	

}*/
USActionComponent* USAction::GetOwningComponent() const
{
	return Cast<USActionComponent>(GetOuter());
}

bool USAction::IsRunning() const
{
	return bIsRunning;
}

void USAction::StartAction_Implementation(AActor* Instigator)
{
	UE_LOG(LogTemp,Warning,TEXT("Running Action: %s"),*GetNameSafe(this));
	USActionComponent* Component = GetOwningComponent();
	Component->ActivateGameplayTags.AppendTags(GrantedTags);
	bIsRunning = true;

	TimeStarted = GetWorld()->TimeSeconds;
	GetOwningComponent()->OnActionStarted.Broadcast(GetOwningComponent(),this);
}
void USAction::StopAction_Implementation(AActor* Instigator)
{
	UE_LOG(LogTemp,Warning,TEXT("Stopped Action: %s"),*GetNameSafe(this));
	USActionComponent* Component = GetOwningComponent();
	Component->ActivateGameplayTags.RemoveTags(GrantedTags);
	bIsRunning = false;

	GetOwningComponent()->OnActionStopped.Broadcast(GetOwningComponent(),this);
}

UWorld* USAction::GetWorld() const
{
	USActionComponent* Comp = Cast<USActionComponent>(GetOuter());
	if (Comp)
	{
		return Comp->GetWorld();
	}
	return nullptr;
}


bool USAction::CanStart_Implementation(AActor* Instigator)
{
	if(IsRunning())
	{
		return false;
	}
	USActionComponent* Comp = GetOwningComponent();
	if(Comp->ActivateGameplayTags.HasAny(BlockedTags))
	{
		return false;
	}
	return true;
}
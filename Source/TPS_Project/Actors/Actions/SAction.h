// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SActionComponent.h"
#include "UObject/Object.h"
#include "GameFramework/Actor.h"
#include "SAction.generated.h"


UCLASS(Blueprintable)
class TPS_PROJECT_API USAction : public UObject
{
	GENERATED_BODY()
	
public:
	bool IsRunning() const;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="Action")
	bool bAutoActivate;

	UFUNCTION(BlueprintNativeEvent)
	void StartAction(AActor* Instigator);
	UFUNCTION(BlueprintNativeEvent)
	void StopAction(AActor* Instigator);
	UFUNCTION(BlueprintNativeEvent)
	bool CanStart(AActor* Instigator);
	UPROPERTY(EditDefaultsOnly,Category="Action")
	FName ActionName;

	virtual UWorld* GetWorld() const override;
	
protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="UI")
	UTexture2D* ActionIcon;

	float TimeStarted;

	UFUNCTION(BlueprintCallable,Category="Action")
	USActionComponent* GetOwningComponent() const;
	UPROPERTY(EditDefaultsOnly, Category="Tags")
	FGameplayTagContainer GrantedTags;
	UPROPERTY(EditDefaultsOnly, Category="Tags")
	FGameplayTagContainer BlockedTags;

	bool bIsRunning;
	
};

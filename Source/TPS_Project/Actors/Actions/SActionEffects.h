// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/Actions/SAction.h"
#include "SActionEffects.generated.h"

/**
 * 
 */
UCLASS()
class TPS_PROJECT_API USActionEffects : public USAction
{
	GENERATED_BODY()

public:
	USActionEffects();
	virtual void StartAction_Implementation(AActor* Instigator) override;
	virtual void StopAction_Implementation(AActor* Instigator) override;
	UFUNCTION(BlueprintCallable,Category="Action")
	float GetTimeRemaining() const;

protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Effects")
	float Duration;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Effects")
	float Period;

	FTimerHandle DurationHandle;
	FTimerHandle PeriodHandle;

	UFUNCTION(BlueprintNativeEvent, Category="Effects")
	void ExecutePeriodEffect(AActor* Instigator);
};

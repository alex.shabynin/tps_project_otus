

#include "Actors/SDoor.h"

#include "TPSTypes.h"
//#include "Components/TimelineComponent.h"

ASDoor::ASDoor()
{
	USceneComponent* DefaultRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DoorRoot"));
	SetRootComponent(DefaultRoot);
	DoorPivot = CreateDefaultSubobject<USceneComponent>(TEXT("DoorPivot"));
	DoorPivot->SetupAttachment(GetRootComponent());

	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMesh"));
	DoorMesh->SetupAttachment(DoorPivot);
	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
}

void ASDoor::BeginPlay()
{
	Super::BeginPlay();

	if(IsValid(DoorAnimationCurve))
	{
		FOnTimelineFloatStatic DoorAnimationDelegate;
		DoorAnimationDelegate.BindUObject(this, &ASDoor::UpdateDoorAnimation);
		DoorOpenTimeline.AddInterpFloat(DoorAnimationCurve,DoorAnimationDelegate);

		FOnTimelineEventStatic DoorOpenedDelegate;
		DoorOpenedDelegate.BindUObject(this, &ASDoor::OnDoorAnimationFinished);
		DoorOpenTimeline.SetTimelineFinishedFunc(DoorOpenedDelegate);
	}	
}

void ASDoor::InteractWithDoor()
{
	SetActorTickEnabled(true);
	if(bIsDoorOpen)
	{
		DoorOpenTimeline.Reverse();
	}
	else
	{
		DoorOpenTimeline.Play();
	}
	bIsDoorOpen = !bIsDoorOpen;
}

void ASDoor::UpdateDoorAnimation(float Alpha)
{
	float YawAngle = FMath::Lerp(AngleClosed,AngleOpened,FMath::Clamp(Alpha,0.0f,1.0f));
	DoorPivot->SetRelativeRotation(FRotator(0.0f,YawAngle,0.0f));
}

void ASDoor::OnDoorAnimationFinished()
{
	SetActorTickEnabled(false);
}

void ASDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DoorOpenTimeline.TickTimeline(DeltaTime);

}

void ASDoor::Interact(ASBaseCharacter* Character)
{
	ensureMsgf(IsValid(DoorAnimationCurve), TEXT("Door animation curve is not set"));
	InteractWithDoor();
	if(OnInteractionEvent.IsBound())
	{
		OnInteractionEvent.Broadcast();
	}
}

FName ASDoor::GetActionEventName() const
{
	return ActionInteract;
}

bool ASDoor::HasOnInteractionCallback() const
{
	return true;
}

FDelegateHandle ASDoor::AddOnInteractionUFunction(UObject* Object, const FName& FunctionName)
{
	return OnInteractionEvent.AddUFunction(Object, FunctionName);
}

void ASDoor::RemoveOnInteractionDelegate(FDelegateHandle DelegateHandle)
{
	OnInteractionEvent.Remove(DelegateHandle);

	
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SPatrollingPath.generated.h"

UCLASS()
class TPS_PROJECT_API ASPatrollingPath : public AActor
{
	GENERATED_BODY()
	
public:	

	const TArray<FVector>& GetWayPoints() const;

protected:

	UPROPERTY(EditInstanceOnly,BlueprintReadOnly,Category="Path", meta=(MakeEditWidget))
	TArray<FVector> WayPoints;

};

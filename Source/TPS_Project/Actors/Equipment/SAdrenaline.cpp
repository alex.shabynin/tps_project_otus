// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Equipment/SAdrenaline.h"

#include "SBaseCharacter.h"

bool USAdrenaline::Consume(ASBaseCharacter* ConsumeTarget)
{
	ConsumeTarget->RestoreFullStamina();
	this->ConditionalBeginDestroy();
	return true;
}

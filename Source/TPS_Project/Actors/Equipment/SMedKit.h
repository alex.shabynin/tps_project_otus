// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SInventoryItem.h"
#include "SMedKit.generated.h"

/**
 * 
 */
UCLASS()
class TPS_PROJECT_API USMedKit : public USInventoryItem
{
	GENERATED_BODY()
public:
	virtual bool Consume(ASBaseCharacter* ConsumeTarget) override;
	
protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="MedKit")
	float Health = 25.0f;
};

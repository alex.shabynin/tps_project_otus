// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/Equipment/SPickable.h"
#include "SPickableWeapon.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TPS_PROJECT_API ASPickableWeapon : public ASPickable
{
	GENERATED_BODY()
public:
	ASPickableWeapon();

	virtual void Interact(ASBaseCharacter* Character) override;
	virtual FName GetActionEventName() const override;

protected:
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	UStaticMeshComponent* WeaponMesh;
	
};

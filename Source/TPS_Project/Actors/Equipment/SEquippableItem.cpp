// Fill out your copyright notice in the Description page of Project Settings.


#include "SEquippableItem.h"


EEquippedItemType ASEquippableItem::GetItemType() const
{
	return ItemType;
}

UAnimMontage* ASEquippableItem::GetCharacterEquipAnimMontage() const
{
	return CharacterEquipAnimMontage;
}

FName ASEquippableItem::GetUnequippedSocketName() const
{
	return UnequippedSocketName;
}

FName ASEquippableItem::GetEquippedSocketName() const
{
	return EquippedSocketName;
}

FName ASEquippableItem::GetDataTableID() const
{
	return DataTableID;
}

bool ASEquippableItem::IsSlotCompatible(EEquipmentSlots Slot)
{
	return CompatibleEquipmentSlots.Contains(Slot);
}

void ASEquippableItem::Equip()
{
	if (OnEquipmentStateChanged.IsBound())
	{
		OnEquipmentStateChanged.Broadcast(true);
	}
}

void ASEquippableItem::UnEquip()
{
	if (OnEquipmentStateChanged.IsBound())
	{
		OnEquipmentStateChanged.Broadcast(false);
	}
}

ASBaseCharacter* ASEquippableItem::GetCharacterOwner() const
{
	return CachedCharacterOwner.IsValid() ? CachedCharacterOwner.Get() : nullptr;
}

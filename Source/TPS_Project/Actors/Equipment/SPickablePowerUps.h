// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/Equipment/SPickable.h"
#include "SPickablePowerUps.generated.h"


UCLASS(Blueprintable)
class TPS_PROJECT_API ASPickablePowerUps : public ASPickable
{
	GENERATED_BODY()

public:
	ASPickablePowerUps();
	virtual void Interact(ASBaseCharacter* Character) override;
	virtual FName GetActionEventName() const override;

protected:
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* PowerUpMesh;
};

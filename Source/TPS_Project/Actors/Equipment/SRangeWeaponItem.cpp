// Fill out your copyright notice in the Description page of Project Settings.


#include "SRangeWeaponItem.h"

#include "SBaseCharacter.h"
#include "SPlayerController.h"
#include "SWeaponBarrelComponent.h"
#include "TPSTypes.h"


ASRangeWeaponItem::ASRangeWeaponItem()
{

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("WeaponRoot"));
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(RootComponent);
	WeaponBarrel = CreateDefaultSubobject<USWeaponBarrelComponent>(TEXT("WeaponBarrel"));
	WeaponBarrel->SetupAttachment(WeaponMesh,SocketWeaponMuzzle);

	EquippedSocketName = SocketCharacterWeapon;
	
}
void ASRangeWeaponItem::BeginPlay()
{
	Super::BeginPlay();
	SetAmmo(MaxAmmo);
}
void ASRangeWeaponItem::StartFire()
{
	bIsFiring = true;
	MakeShot();
	if(WeaponFireMode == EWeaponFireMode::AutoFire)
	{
		GetWorld()->GetTimerManager().ClearTimer(ShotTimer);
		GetWorld()->GetTimerManager().SetTimer(ShotTimer,this, &ASRangeWeaponItem::MakeShot,GetShotTimerInterval(),true);
	}
}

void ASRangeWeaponItem::StopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(ShotTimer);
	bIsFiring = false;
}

void ASRangeWeaponItem::StartAim()
{
	bIsAiming = true;
}

void ASRangeWeaponItem::StopAim()
{
	bIsAiming = false;
}

void ASRangeWeaponItem::StartReload()
{
	checkf(GetOwner()->IsA<ASBaseCharacter>(), TEXT("ASRangeWeaponItem::StartReload can be used only with ASBaseCharacter"));
	ASBaseCharacter* CharacterOwner = StaticCast<ASBaseCharacter*>(GetOwner());

	bIsReloading = true;
	if(IsValid(CharacterReloadMontage))
	{
		float MontageDuration = CharacterOwner->PlayAnimMontage(CharacterReloadMontage);
		PlayAnimMontage(WeaponReloadMontage);
		GetWorld()->GetTimerManager().SetTimer(ReloadTimer,[this](){ EndReload(true); },MontageDuration,false);
	}
	else
	{
		EndReload(true);
	}
}

void ASRangeWeaponItem::EndReload(bool bIsSuccess)
{
	if(!bIsReloading)
	{
		return;
	}

	if(!bIsSuccess)
	{
		checkf(GetOwner()->IsA<ASBaseCharacter>(), TEXT("ASRangeWeaponItem::StartReload can be used only with ASBaseCharacter"));
		ASBaseCharacter* CharacterOwner = StaticCast<ASBaseCharacter*>(GetOwner());
		CharacterOwner->StopAnimMontage(CharacterReloadMontage);
		StopAnimMontage(WeaponReloadMontage);

	}
	GetWorld()->GetTimerManager().ClearTimer(ReloadTimer);
	
	bIsReloading = false;
	if(bIsSuccess && OnReloadComplete.IsBound())
	{
		OnReloadComplete.Broadcast();
	}
}

bool ASRangeWeaponItem::IsFiring() const
{
	return bIsFiring;
}

bool ASRangeWeaponItem::IsReloading() const
{
	return bIsReloading;
}

int32 ASRangeWeaponItem::GetAmmo() const
{
	return Ammo;
}

int32 ASRangeWeaponItem::GetMaxAmmo() const
{
	return MaxAmmo;
}

void ASRangeWeaponItem::SetAmmo(int32 NewAmmo)
{
	Ammo = NewAmmo;
	if(OnAmmoChanged.IsBound())
	{
		OnAmmoChanged.Broadcast(Ammo);
	}
}

EAmmunitionType ASRangeWeaponItem::GetAmmoType() const
{
	return AmmoType;
}

bool ASRangeWeaponItem::CanShoot() const
{
	return Ammo > 0;
}

float ASRangeWeaponItem::GetAimFOV() const
{
	return AimFOV;
}

float ASRangeWeaponItem::GetAimMovingSpeed() const
{
	return AimMovingSpeed;
}

FTransform ASRangeWeaponItem::GetForeGripTransform() const
{
	return WeaponMesh->GetSocketTransform(SocketWeaponGripHand);
}



float ASRangeWeaponItem::GetCurrentBulletSpreadAngle() const
{
		//return bIsAiming ? AimSpreadAngle : SpreadAngle; //Так было исходно
		float AngleInDegrees = bIsAiming ? AimSpreadAngle : SpreadAngle;
		return FMath::DegreesToRadians(AngleInDegrees);
}

void ASRangeWeaponItem::MakeShot()
{
	checkf(GetOwner()->IsA<ASBaseCharacter>(), TEXT("ASRangeWeaponItem::Fire can be used only with ASBaseCharacter"));
	ASBaseCharacter* CharacterOwner = StaticCast<ASBaseCharacter*>(GetOwner());

	if(!CanShoot())
	{
		StopFire();
		if(Ammo == 0 && bAutoReload)
		{
			CharacterOwner->Reload();
		}
		return;
	}

	EndReload(false);
	
	CharacterOwner->PlayAnimMontage(CharacterFireMontage);
	PlayAnimMontage(WeaponFireMontage);
	
	FVector ShotLocation;
	FRotator ShotRotation;

	if(CharacterOwner->IsPlayerControlled())
	{
		ASPlayerController* Controller = CharacterOwner->GetController<ASPlayerController>();
		Controller->GetPlayerViewPoint(ShotLocation, ShotRotation);

	}
	else
	{
		ShotLocation = WeaponBarrel->GetComponentLocation();
		ShotRotation = CharacterOwner->GetBaseAimRotation();
		
	}
	
	FVector ShotDirection = ShotRotation.RotateVector(FVector::ForwardVector);
	//ViewDirection += GetBulletSpreadOffset(FMath::RandRange(0.0f,FMath::DegreesToRadians(GetCurrentBulletSpreadAngle())),PlayerViewRotation); //Вернуть если не прокатит
	SetAmmo(Ammo - 1);
	WeaponBarrel->Shot(ShotLocation, ShotDirection, GetCurrentBulletSpreadAngle());
	//WeaponBarrel->Shot(PlayerViewPoint, ViewDirection, Controller); //Вернуть если не прокатит

}

FVector ASRangeWeaponItem::GetBulletSpreadOffset(float Angle, FRotator ShotRotation) const
{
	float SpreadSize = FMath::Tan(Angle);
	float RotationAngle = FMath::RandRange(0.0f, 2* PI);

	float SpreadY = FMath::Cos(RotationAngle);
	float SpreadZ = FMath::Sin(RotationAngle);

	FVector Result = (ShotRotation.RotateVector(FVector::UpVector) * SpreadZ
						+ ShotRotation.RotateVector(FVector::RightVector) * SpreadY) * SpreadSize;

	return Result;
}

float ASRangeWeaponItem::GetShotTimerInterval() const
{
	return 60.0f / RateOfFire;
}

float ASRangeWeaponItem::PlayAnimMontage(UAnimMontage* AnimMontage)
{
	UAnimInstance* WeaponAnimInstance = WeaponMesh->GetAnimInstance();
	float Result = 0.0f;
	if(IsValid(WeaponAnimInstance))
	{
		Result = WeaponAnimInstance->Montage_Play(AnimMontage);
	}
	return Result;
}

void ASRangeWeaponItem::StopAnimMontage(UAnimMontage* AnimMontage, float BlendOutTime)
{
	UAnimInstance* WeaponAnimInstance = WeaponMesh->GetAnimInstance();
	if(IsValid(WeaponAnimInstance))
	{
		WeaponAnimInstance->Montage_Stop(BlendOutTime, AnimMontage);
	}
}

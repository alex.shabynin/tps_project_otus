// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SInventoryItem.h"
#include "SAdrenaline.generated.h"

/**
 * 
 */
UCLASS()
class TPS_PROJECT_API USAdrenaline : public USInventoryItem
{
	GENERATED_BODY()
public:
	virtual bool Consume(ASBaseCharacter* ConsumeTarget) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactive.h"
#include "GameFramework/Actor.h"
#include "SPickable.generated.h"

UCLASS(Abstract, NotBlueprintable)
class TPS_PROJECT_API ASPickable : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	const FName& GetDataTableID() const;

protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Items")
	FName DataTableID =NAME_None;

};

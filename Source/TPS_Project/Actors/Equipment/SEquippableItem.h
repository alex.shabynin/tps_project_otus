// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPSTypes.h"
#include "CharacterEquipmentComponent.h"
#include "GameFramework/Actor.h"
#include "SEquippableItem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEquipmentStateChanged, bool, bIsEquipped);

class UAnimMontage;
UCLASS(Abstract,NotBlueprintable)
class TPS_PROJECT_API ASEquippableItem : public AActor
{
	GENERATED_BODY()

public:
	EEquippedItemType GetItemType() const;
	UAnimMontage* GetCharacterEquipAnimMontage() const;
	FName GetUnequippedSocketName() const;
	FName GetEquippedSocketName() const;
	FName GetDataTableID() const;

	bool IsSlotCompatible(EEquipmentSlots Slot);

	virtual void Equip();
	virtual void UnEquip();
	
protected:
	UPROPERTY(BlueprintAssignable)
	FOnEquipmentStateChanged OnEquipmentStateChanged;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Equippable item")
	EEquippedItemType ItemType = EEquippedItemType::None;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Equippable item")
	UAnimMontage* CharacterEquipAnimMontage;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Equippable item")
	FName UnequippedSocketName = NAME_None;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Equippable item")
	FName EquippedSocketName = NAME_None;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Equipable item")
	TArray<EEquipmentSlots> CompatibleEquipmentSlots;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Items")
	FName DataTableID = NAME_None;

	ASBaseCharacter* GetCharacterOwner() const;
	
private:
	TWeakObjectPtr<ASBaseCharacter> CachedCharacterOwner;
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Equipment/SMedKit.h"

#include "SBaseCharacter.h"
#include "SCharacterAttributesComponent.h"

bool USMedKit::Consume(ASBaseCharacter* ConsumeTarget)
{
	USCharacterAttributesComponent* CharacterAttributes = ConsumeTarget->GetCharacterAttributesComponent();
	CharacterAttributes->AddHealth(Health);
	this->ConditionalBeginDestroy();
	return true;
}

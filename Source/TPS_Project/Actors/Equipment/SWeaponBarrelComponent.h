// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SRangeWeaponItem.h"
#include "Components/SceneComponent.h"
#include "SWeaponBarrelComponent.generated.h"

UENUM(BlueprintType)
enum class EHitRegistrationType : uint8
{
	HitScan,
	Projectile
};

USTRUCT(BlueprintType)
struct FDecalInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Decal Info")
	UMaterialInterface* DecalMaterial;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Decal Info")
	FVector DecalSize = FVector(5.0f,5.0f,5.0f);
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Decal Info")
	float DecalLifeTime = 10.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Decal Info")
	float DecalFadeOutTime = 5.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Decal Info")
	float DecalFadeScreenSize = 0.001f;

};

class UNiagaraSystem;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_PROJECT_API USWeaponBarrelComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	void Shot(FVector ShotStart, FVector ShotDirection, float SpreadAngle);
	//void Shot(FVector ShotStart, FVector ShotDirection, AController* Controller); //Вернуть если не прокатит

UFUNCTION(BlueprintImplementableEvent)
	void CreateFields(const FVector& FieldLocation);

protected:
	
UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Barrel attributes")
	float FireRange = 8000.0f;
UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Barell attributes", meta = (ClampMin = 1, UIMin = 1))
	int32 BulletsPerShot = 1;
UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Barell attributes | Hit registration")
	EHitRegistrationType HitRegistration = EHitRegistrationType::HitScan;
UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Barrel attributes | Damage")
	float DamageAmount = 25.0f;
UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Barell attributes | Damage")
	TSubclassOf<class UDamageType> DamageTypeClass;
UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Barrel attributes | VFX")
	UNiagaraSystem* MuzzleFlashFX;
UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Barrel attributes | VFX")
	UNiagaraSystem* TraceFX;
UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Barrel attributes | Decals")
	FDecalInfo DefaultDecalInfo;
	

private:
	APawn* GetOwningPawn() const;
	AController* GetController() const;

	UFUNCTION()
	void ProcessHit(const FHitResult& HitResult, const FVector& Direction);
	bool HitScan(FVector ShotStart, OUT FVector& ShotEnd, FVector ShotDirection);
	
	
	FVector GetBulletSpreadOffset(float Angle, FRotator ShotRotation) const;

};

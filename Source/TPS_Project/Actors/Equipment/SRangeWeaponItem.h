// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SEquippableItem.h"
#include "SRangeWeaponItem.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnReloadComplete);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnAmmoChanged, int32);

UENUM(BlueprintType)
enum class EWeaponFireMode : uint8
{
	SingleFire,
	AutoFire
};

/**
 * 
 */
class USkeletalMeshComponent;
class USWeaponBarrelComponent;
class UAnimMontage;

UCLASS(Blueprintable)
class TPS_PROJECT_API ASRangeWeaponItem : public ASEquippableItem
{
	GENERATED_BODY()

public:
	ASRangeWeaponItem();
	void StartFire();
	void StopFire();
	void StartAim();
	void StopAim();

	void StartReload();
	void EndReload(bool bIsSuccess);

	bool IsFiring() const;
	bool IsReloading() const;
	
	int32 GetAmmo() const;
	int32 GetMaxAmmo() const;
	void SetAmmo(int32 NewAmmo);
	EAmmunitionType GetAmmoType() const;
	bool CanShoot() const;
	float GetAimFOV() const;
	float GetAimMovingSpeed() const;

	FTransform GetForeGripTransform() const;

	FOnAmmoChanged OnAmmoChanged;
	FOnReloadComplete OnReloadComplete;

	
protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category="Components")
	USkeletalMeshComponent* WeaponMesh;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category="Components")
	USWeaponBarrelComponent* WeaponBarrel;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Animations | Weapon")
	UAnimMontage* WeaponFireMontage;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Animations | Weapon")
	UAnimMontage* WeaponReloadMontage;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Animations | Character")
	UAnimMontage* CharacterFireMontage;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Animations | Character")
	UAnimMontage* CharacterReloadMontage;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Weapon | Parameters")
	EWeaponFireMode WeaponFireMode = EWeaponFireMode::SingleFire;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Weapon | Parameters", meta=(ClampMin=1.0f,UIMin=1.0f))
	float RateOfFire = 600.0f;//Выстрелов в минуту
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Weapon | Parameters", meta=(ClampMin=0.0f,UIMin=0.0f,ClampMax=2.0f,UIMax=2.0f))
	float SpreadAngle = 1.0f;//Разброс пуль в градусах
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Weapon | Parameters | Aiming", meta=(ClampMin=0.0f,UIMin=0.0f,ClampMax=2.0f,UIMax=2.0f))
	float AimSpreadAngle = 0.35f;//Разброс пуль в градусах при прицеливании
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Weapon | Parameters | Aiming", meta=(ClampMin=0.0f,UIMin=0.0f))
	float AimMovingSpeed = 200.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Weapon | Parameters | Aiming", meta=(ClampMin=0.0f,UIMin=0.0f))
	float AimFOV = 60.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Weapon | Parameters | Ammo", meta=(ClampMin=1,UIMin=1))
	EAmmunitionType AmmoType;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Weapon | Parameters | Ammo", meta=(ClampMin=1,UIMin=1))
	int32 MaxAmmo = 30;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Weapon | Parameters | Ammo")
	bool bAutoReload = true;
	

private:
	int32 Ammo = 0;
	bool bIsAiming;
	bool bIsReloading = false;
	bool bIsFiring = false;
	float GetCurrentBulletSpreadAngle() const;
	void MakeShot();
	FVector GetBulletSpreadOffset(float Angle, FRotator ShotRotation) const;
	float GetShotTimerInterval() const;
	float PlayAnimMontage(UAnimMontage* AnimMontage);
	void StopAnimMontage(UAnimMontage* AnimMontage, float BlendOutTime = 0.0f);
	FTimerHandle ShotTimer;
	FTimerHandle ReloadTimer;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Equipment/SPickablePowerUps.h"

#include "SBaseCharacter.h"
#include "SDataTableUtils.h"
#include "SInventoryItem.h"
#include "TPSTypes.h"

ASPickablePowerUps::ASPickablePowerUps()
{
	PowerUpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PowerUpMesh"));
	SetRootComponent(PowerUpMesh);
}

void ASPickablePowerUps::Interact(ASBaseCharacter* Character)
{
	FItemTableRow* ItemData =SDataTableUtils::FindInventoryItemData(GetDataTableID());

	if (ItemData == nullptr)
	{
		return;
	}

	TWeakObjectPtr<USInventoryItem> Item = TWeakObjectPtr<USInventoryItem>(NewObject<USInventoryItem>(Character, ItemData->InventoryItemClass));
	Item->Initialize(DataTableID, ItemData->InventoryItemDescription);

	const bool bPickedUp = Character->PickupItem(Item);
	if (bPickedUp)
	{
		Destroy();
	}
}

FName ASPickablePowerUps::GetActionEventName() const
{
	return ActionInteract;
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "SWeaponBarrelComponent.h"

#include "TPSTypes.h"
#include "DrawDebugHelpers.h"
#include "TPSDebugSubsystem.h"
#include "Engine/DamageEvents.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraComponent.h"
#include "Components/DecalComponent.h"

/*void USWeaponBarrelComponent::Shot(FVector ShotStart, FVector ShotDirection, AController* Controller)
{
	FVector MuzzleLocation = GetComponentRotation().RotateVector(FVector::ForwardVector);
	
	FVector ShotEnd = ShotStart + FireRange * ShotDirection;

	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),MuzzleFlashFX,GetComponentLocation(),GetComponentRotation());
	//UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),MuzzleFlashFX,MuzzleLocation,GetComponentRotation());


#if	ENABLE_DRAW_DEBUG
	UTPSDebugSubsystem* DebugSubSystem = UGameplayStatics::GetGameInstance(GetWorld())->GetSubsystem<UTPSDebugSubsystem>();
	bool bIsDebugEnabled = DebugSubSystem->IsCategoryEnabled(DebugCategoryRangeWeapon);
#else
	bool bIsDebugEnabled = false;
#endif
	
	FHitResult ShotResult;
	if(GetWorld()->LineTraceSingleByChannel(ShotResult,ShotStart,ShotEnd,ECC_Bullet))
	{
		ShotEnd = ShotResult.ImpactPoint;
		if(bIsDebugEnabled)
		{
			DrawDebugSphere(GetWorld(),ShotEnd,20.0f,32,FColor::Red,false,1.0f);
		}
		AActor* HitActor = ShotResult.GetActor();
		if(IsValid(HitActor))
		{
			HitActor->TakeDamage(DamageAmount, FDamageEvent(), Controller, GetOwner());
		}
	UDecalComponent* DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(),DefaultDecalInfo.DecalMaterial,DefaultDecalInfo.DecalSize,ShotResult.ImpactPoint,ShotResult.ImpactNormal.ToOrientationRotator());
		if(IsValid(DecalComponent))
		{
			DecalComponent->SetFadeScreenSize(DefaultDecalInfo.DecalFadeScreenSize);
			DecalComponent->SetFadeOut(DefaultDecalInfo.DecalLifeTime,DefaultDecalInfo.DecalFadeOutTime);
		}
	UNiagaraComponent* TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),TraceFX,GetComponentLocation(),GetComponentRotation());
	TraceFXComponent->SetVectorParameter(FXParamTraceEnd,ShotEnd);
		/*UNiagaraComponent* TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),TraceFX,MuzzleLocation,GetComponentRotation());
		TraceFXComponent->SetVectorParameter(FXParamTraceEnd,ShotEnd);	#1#
	
	}
	if(bIsDebugEnabled)
	{
		//DrawDebugLine(GetWorld(),MuzzleLocation,ShotEnd,FColor::Red,false,1.0f,0,3.0f);
		DrawDebugLine(GetWorld(),GetComponentLocation(),ShotEnd,FColor::Red,false,1.0f,0,3.0f);
	}
}*/

void USWeaponBarrelComponent::Shot(FVector ShotStart, FVector ShotDirection, float SpreadAngle)
{
	FVector MuzzleLocation = GetComponentLocation();

	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),MuzzleFlashFX,MuzzleLocation,GetComponentRotation());
	//UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),MuzzleFlashFX,MuzzleLocation,GetComponentRotation());
	for(int i=0; i < BulletsPerShot; i++)
	{
		ShotDirection += GetBulletSpreadOffset(FMath::RandRange(0.0f, SpreadAngle), ShotDirection.ToOrientationRotator());
		ShotDirection = ShotDirection.GetSafeNormal();

		FVector ShotEnd = ShotStart + FireRange * ShotDirection;
		
#if	ENABLE_DRAW_DEBUG
		UTPSDebugSubsystem* DebugSubSystem = UGameplayStatics::GetGameInstance(GetWorld())->GetSubsystem<UTPSDebugSubsystem>();
		bool bIsDebugEnabled = DebugSubSystem->IsCategoryEnabled(DebugCategoryRangeWeapon);
#else
		bool bIsDebugEnabled = false;
#endif

		switch(HitRegistration)
		{
		case EHitRegistrationType::HitScan:
			{
				bool bHasHit = HitScan(ShotStart,ShotEnd,ShotDirection);
				if(bIsDebugEnabled && bHasHit)
				{
					DrawDebugSphere(GetWorld(),ShotEnd,10.0f,24,FColor::Red,false,1.0f);
					
				}
				break;
			}
		case EHitRegistrationType::Projectile:
			{
				//TODO launch projectile
				break;
			}
		}
		UNiagaraComponent* TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),TraceFX,MuzzleLocation,GetComponentRotation());
        			TraceFXComponent->SetVectorParameter(FXParamTraceEnd,ShotEnd);

		if(bIsDebugEnabled)
		{
			DrawDebugLine(GetWorld(),MuzzleLocation,ShotEnd,FColor::Red,false,1.0f,0,3.0f);
		}
		/*FHitResult ShotResult;
		if(GetWorld()->LineTraceSingleByChannel(ShotResult,ShotStart,ShotEnd,ECC_Bullet))
		{
			ShotEnd = ShotResult.ImpactPoint;
			if(bIsDebugEnabled)
			{
				DrawDebugSphere(GetWorld(),ShotEnd,20.0f,32,FColor::Red,false,1.0f);
			}
			AActor* HitActor = ShotResult.GetActor();
			if(IsValid(HitActor))
			{
				HitActor->TakeDamage(DamageAmount, FDamageEvent(), SpreadAngle, GetOwner());
			}
			UDecalComponent* DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(),DefaultDecalInfo.DecalMaterial,DefaultDecalInfo.DecalSize,ShotResult.ImpactPoint,ShotResult.ImpactNormal.ToOrientationRotator());
			if(IsValid(DecalComponent))
			{
				DecalComponent->SetFadeScreenSize(DefaultDecalInfo.DecalFadeScreenSize);
				DecalComponent->SetFadeOut(DefaultDecalInfo.DecalLifeTime,DefaultDecalInfo.DecalFadeOutTime);
			}
			
			/*UNiagaraComponent* TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),TraceFX,MuzzleLocation,GetComponentRotation());
			TraceFXComponent->SetVectorParameter(FXParamTraceEnd,ShotEnd);	#1#
	
		}*/
		/*if(bIsDebugEnabled)
		{
			//DrawDebugLine(GetWorld(),MuzzleLocation,ShotEnd,FColor::Red,false,1.0f,0,3.0f);
			DrawDebugLine(GetWorld(),GetComponentLocation(),ShotEnd,FColor::Red,false,1.0f,0,3.0f);
		}*/
	}
}



bool USWeaponBarrelComponent::HitScan(FVector ShotStart, OUT FVector& ShotEnd, FVector ShotDirection)
	{
		FHitResult ShotResult;
		bool bHasHit = GetWorld()->LineTraceSingleByChannel(ShotResult, ShotStart, ShotEnd, ECC_Bullet);
		if (bHasHit)
		{
			ShotEnd = ShotResult.ImpactPoint;
			ProcessHit(ShotResult, ShotDirection);

		}

		return bHasHit;
	}
	
APawn* USWeaponBarrelComponent::GetOwningPawn() const
{
	APawn* PawnOwner = Cast<APawn>(GetOwner());
	if (IsValid(PawnOwner))
	{
		PawnOwner = Cast<APawn>(GetOwner()->GetOwner());
	}
	return PawnOwner;
}

AController* USWeaponBarrelComponent::GetController() const
{
	APawn* PawnOwner = GetOwningPawn();
	return IsValid(PawnOwner) ? PawnOwner->GetController() : nullptr;
}

void USWeaponBarrelComponent::ProcessHit(const FHitResult& HitResult, const FVector& Direction)
{
		AActor* HitActor = HitResult.GetActor();
		if (IsValid(HitActor))
		{
			FPointDamageEvent DamageEvent;
			DamageEvent.HitInfo = HitResult;
			DamageEvent.ShotDirection = Direction;
			DamageEvent.DamageTypeClass = DamageTypeClass;
			HitActor->TakeDamage(DamageAmount, DamageEvent, GetController(), GetOwner());
		}

		UDecalComponent* DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), DefaultDecalInfo.DecalMaterial, DefaultDecalInfo.DecalSize, HitResult.ImpactPoint, HitResult.ImpactNormal.ToOrientationRotator());
		if (IsValid(DecalComponent))
		{
			DecalComponent->SetFadeScreenSize(DefaultDecalInfo.DecalFadeScreenSize);
			DecalComponent->SetFadeOut(DefaultDecalInfo.DecalLifeTime, DefaultDecalInfo.DecalFadeOutTime);
		}
	
}
	
FVector USWeaponBarrelComponent::GetBulletSpreadOffset(float Angle, FRotator ShotRotation) const
{
	float SpreadSize = FMath::Tan(Angle);
	float RotationAngle = FMath::RandRange(0.0f, 2 * PI);

	float SpreadY = FMath::Cos(RotationAngle);
	float SpreadZ = FMath::Sin(RotationAngle);

	FVector Result = (ShotRotation.RotateVector(FVector::UpVector) * SpreadZ 
					+ ShotRotation.RotateVector(FVector::RightVector) * SpreadY) * SpreadSize;

	return Result;
}











// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Equipment/SPickableWeapon.h"

#include "SBaseCharacter.h"
#include "SDataTableUtils.h"
#include "TPSTypes.h"
#include "WeaponInventoryItem.h"
//#include "Engine/DataTable.h"

ASPickableWeapon::ASPickableWeapon()
{
	WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon"));
	SetRootComponent(WeaponMesh);
	
}

void ASPickableWeapon::Interact(ASBaseCharacter* Character)
{
	FWeaponTableRow* WeaponRow = SDataTableUtils::FindWeaponData(DataTableID);
	if(WeaponRow)
	{
		//Character->AddEquipmentItem(WeaponRow->EquippableActor);
		TWeakObjectPtr<UWeaponInventoryItem> Weapon = NewObject<UWeaponInventoryItem>(Character);
		Weapon->Initialize(DataTableID,WeaponRow->WeaponItemDescription);
		Weapon->SetEquipWeaponClass(WeaponRow->EquippableActor);
		Character->PickupItem(Weapon);
		Destroy();
	}
}

FName ASPickableWeapon::GetActionEventName() const
{
	return ActionInteract;
	
}

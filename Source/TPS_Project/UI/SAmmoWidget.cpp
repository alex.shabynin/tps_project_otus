// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/SAmmoWidget.h"

void USAmmoWidget::UpdateAmmoCount(int32 NewAmmo, int32 NewTotalAmmo)
{
	Ammo = NewAmmo;
	TotalAmmo = NewTotalAmmo;
}

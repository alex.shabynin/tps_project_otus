// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SAmmoWidget.generated.h"

/**
 * 
 */
UCLASS()
class TPS_PROJECT_API USAmmoWidget : public UUserWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Ammo")
	int32 Ammo;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Ammo")
	int32 TotalAmmo;

private:
	UFUNCTION()
	void UpdateAmmoCount(int32 NewAmmo, int32 NewTotalAmmo);
	
};

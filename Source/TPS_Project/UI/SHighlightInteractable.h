// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SHighlightInteractable.generated.h"


class UTextBlock;

UCLASS()
class TPS_PROJECT_API USHighlightInteractable : public UUserWidget
{
	GENERATED_BODY()

public:

	void SetActionText(FName Keyname);

protected:
	UPROPERTY(meta=(BindWidget))
	UTextBlock* ActionText;
	
};

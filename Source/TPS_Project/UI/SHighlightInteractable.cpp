// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/SHighlightInteractable.h"

#include "Components/TextBlock.h"

void USHighlightInteractable::SetActionText(FName Keyname)
{
	if(IsValid(ActionText))
	{
		ActionText->SetText(FText::FromName(Keyname));
	}
}

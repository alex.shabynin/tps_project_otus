// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SAmmoWidget.h"
#include "SHighlightInteractable.h"
#include "SReticleWidget.h"
#include "Blueprint/UserWidget.h"
#include "SPlayerHUDWidget.generated.h"

/**
 * 
 */

UCLASS()
class TPS_PROJECT_API USPlayerHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	USReticleWidget* GetReticleWidget();
	USAmmoWidget* GetAmmoWidget();

	void SetHighlightInteractableVisibility(bool bIsVisible);

	void SetHighlightInteractableActionText(FName KeyName);

protected:
	UFUNCTION(BlueprintCallable)
	float GetHealthPercent() const;
	UFUNCTION(BlueprintCallable)
	float GetHealth() const;
	UFUNCTION(BlueprintCallable)
	float GetStaminaPercent() const;
	UFUNCTION(BlueprintCallable)
	float GetStamina() const;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Widget Names")
	FName ReticleWidgetName;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category="Widget Names")
	FName AmmoWidgetName;
	UPROPERTY(meta=(BindWidget))
	USHighlightInteractable* InteractableKey;
};

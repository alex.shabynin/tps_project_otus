// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SCharacterInventoryComponent.h"
#include "SInventoryViewWidget.generated.h"

class USInventorySlotWidget;
struct FInventorySlot;
class UGridPanel;

UCLASS()
class TPS_PROJECT_API USInventoryViewWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void InitializeViewWidget(TArray<FInventorySlot>& InventorySlots);
	
protected:
	
	UPROPERTY(meta = (BindWidget))
	UGridPanel* GridPanelItemSlots;
	UPROPERTY(EditDefaultsOnly, Category = "ItemContainer View Settings")
	TSubclassOf<USInventorySlotWidget> InventorySlotWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "ItemContainer View Settings")
	int32 ColumnCount = 4;

	void AddItemSlotView(FInventorySlot& SlotToAdd);
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/SPlayerHUDWidget.h"

#include "SBaseCharacter.h"
#include "SCharacterAttributesComponent.h"
#include "Blueprint/WidgetTree.h"

USReticleWidget* USPlayerHUDWidget::GetReticleWidget()
{
	return WidgetTree->FindWidget<USReticleWidget>(ReticleWidgetName);
}

USAmmoWidget* USPlayerHUDWidget::GetAmmoWidget()
{
	return WidgetTree->FindWidget<USAmmoWidget>(AmmoWidgetName);
}

void USPlayerHUDWidget::SetHighlightInteractableVisibility(bool bIsVisible)
{
	if(!IsValid(InteractableKey))
	{
		return;
	}

	if(bIsVisible)
	{
		InteractableKey->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		InteractableKey->SetVisibility(ESlateVisibility::Hidden);
	}
}

void USPlayerHUDWidget::SetHighlightInteractableActionText(FName KeyName)
{
	if(IsValid(InteractableKey))
	{
		InteractableKey->SetActionText(KeyName);
	}
}

float USPlayerHUDWidget::GetHealthPercent() const
{
	float Result = 1.0f;
	APawn* Pawn = GetOwningPlayerPawn();
	ASBaseCharacter* BaseCharacter = Cast<ASBaseCharacter>(Pawn);
	if(IsValid(BaseCharacter))
	{
		const USCharacterAttributesComponent* CharacterAttributes = BaseCharacter->GetCharacterAttributesComponent();
		Result = CharacterAttributes->GetHealthPercent();
	}
	return Result;
}

float USPlayerHUDWidget::GetHealth() const
{
	float Result = 1.0f;
	APawn* Pawn = GetOwningPlayerPawn();
	ASBaseCharacter* BaseCharacter = Cast<ASBaseCharacter>(Pawn);
	if(IsValid(BaseCharacter))
	{
		const USCharacterAttributesComponent* CharacterAttributes = BaseCharacter->GetCharacterAttributesComponent();
		Result = CharacterAttributes->GetHealth();
	}
	return Result;
}

float USPlayerHUDWidget::GetStaminaPercent() const
{
	float Result = 1.0f;
	APawn* Pawn = GetOwningPlayerPawn();
	ASBaseCharacter* BaseCharacter = Cast<ASBaseCharacter>(Pawn);
	if(IsValid(BaseCharacter))
	{
		Result = BaseCharacter->GetStaminaPercent();
	}
	return Result;
}

float USPlayerHUDWidget::GetStamina() const
{
	float Result = 1.0f;
	APawn* Pawn = GetOwningPlayerPawn();
	ASBaseCharacter* BaseCharacter = Cast<ASBaseCharacter>(Pawn);
	if(IsValid(BaseCharacter))
	{
		Result = BaseCharacter->GetStamina();
	}
	return Result;
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/SCharacterInventoryComponent.h"

#include "SInventoryItem.h"
#include "SInventoryViewWidget.h"
#include "SPlayerController.h"


void FInventorySlot::BindOnInventorySlotUpdate(const FInventorySlotUpdate& Callback) const
{
	OnInventorySlotUpdate = Callback;
}

void FInventorySlot::UnbindOnInventorySlotUpdate()
{
	OnInventorySlotUpdate.Unbind();
}

void FInventorySlot::UpdateSlotState()
{
	OnInventorySlotUpdate.ExecuteIfBound();
}

void FInventorySlot::ClearSlot()
{
	Item = nullptr;
	Count = 0;
	UpdateSlotState();
}

void USCharacterInventoryComponent::OpenViewInventory(ASPlayerController* PlayerController)
{
	if(!IsValid(InventoryViewWidget))
	{
		CreateViewWidget(PlayerController);
	}

	if(!InventoryViewWidget->IsVisible())
	{
		InventoryViewWidget->AddToViewport();
	}

}

void USCharacterInventoryComponent::CloseViewInventory()
{
	if(InventoryViewWidget->IsVisible())
	{
		InventoryViewWidget->RemoveFromParent();
	}
}

bool USCharacterInventoryComponent::IsViewVisible() const
{
	bool Result = false;
	if(IsValid(InventoryViewWidget))
	{
		Result = InventoryViewWidget->IsVisible();
	}

	return Result;
}

int32 USCharacterInventoryComponent::GetCapacity() const
{
	return Capacity;
}

bool USCharacterInventoryComponent::HasFreeSlot() const
{
	return ItemsInInventory < Capacity;
}

bool USCharacterInventoryComponent::AddItem(TWeakObjectPtr<USInventoryItem> ItemToAdd, int32 Count)
{
	if(!ItemToAdd.IsValid() || Count < 0)
	{
		return false;
	}
	bool Result = false;
FInventorySlot* FreeSlot = FindFreeSlot();
	
	if(FreeSlot != nullptr)
	{
		FreeSlot->Item = ItemToAdd;
		FreeSlot->Count = Count;
		ItemsInInventory++;
		Result = true;
		FreeSlot->UpdateSlotState();
	}
	return Result;
}

bool USCharacterInventoryComponent::RemoveItem(FName ItemID)
{
	FInventorySlot* ItemSlot = FindItemSlot(ItemID);
	if(ItemSlot !=nullptr)
	{
		InventorySlots.RemoveAll([=](const FInventorySlot& Slot) {return Slot.Item->GetDataTableID() == ItemID; });
		return true;
	}
	return false;
}

TArray<FInventorySlot> USCharacterInventoryComponent::GetAllItemsCopy() const
{
	return InventorySlots;
}

TArray<FText> USCharacterInventoryComponent::GetAllItemsNames() const
{
	TArray<FText> Result;
	for (const FInventorySlot& Slot : InventorySlots)
	{
		if(Slot.Item.IsValid())
		{
			Result.Add(Slot.Item->GetDescription().Name);
		}
	}
	return Result;
}

void USCharacterInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
	InventorySlots.AddDefaulted(Capacity);
	
}

void USCharacterInventoryComponent::CreateViewWidget(ASPlayerController* PlayerController)
{
	if(IsValid(InventoryViewWidget))
	{
		return;
	}
	if(!IsValid(PlayerController) || !IsValid(InventoryViewWidgetClass))
	{
		return;
	}
	InventoryViewWidget = CreateWidget<USInventoryViewWidget>(PlayerController, InventoryViewWidgetClass);
	InventoryViewWidget->InitializeViewWidget(InventorySlots);
}

FInventorySlot* USCharacterInventoryComponent::FindItemSlot(FName ItemID)
{
	return InventorySlots.FindByPredicate([=](const FInventorySlot& Slot){return Slot.Item->GetDataTableID() ==ItemID;});
}

FInventorySlot* USCharacterInventoryComponent::FindFreeSlot()
{
	return InventorySlots.FindByPredicate([=](const FInventorySlot& Slot){return !Slot.Item.IsValid();});

}





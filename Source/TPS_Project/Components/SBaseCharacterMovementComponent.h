// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LedgeDetectorComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "SBaseCharacterMovementComponent.generated.h"

/**
 * 
 */

struct FMantlingMovementParameters
{
	FVector InitialLocation = FVector::ZeroVector;
	FRotator InitialRotation = FRotator::ZeroRotator;

	FVector TargetLocation = FVector::ZeroVector;
	FRotator TargetRotation = FRotator::ZeroRotator;

	FVector InitialAnimationLocation = FVector::ZeroVector;

	float Duration = 1.0f;
	float StartTime = 0.0f;
	UCurveVector* MantlingCurve;
};

UENUM(BlueprintType)
enum class ECustomMovementMode : uint8
{
	CMOVE_None = 0 UMETA(DisplayName = "None"),
	CMOVE_Mantling UMETA(DisplayName = "Mantling"),
	CMOVE_Max UMETA(Hidden)
};

UCLASS()
class TPS_PROJECT_API USBaseCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
public:
	FORCEINLINE bool IsSprinting() { return bIsSprinting; }

	UFUNCTION(BlueprintCallable)
	bool IsOutOfStamina() const { return bIsOutOfStamina; }

	void SetIsOutOfStamina(bool bIsOutOfStamina_In) { bIsOutOfStamina = bIsOutOfStamina_In; }
	virtual float GetMaxSpeed() const override;

	void StartSprint();
	void StopSprint();
	void StartMantle(const FMantlingMovementParameters& MantlingParameters);
	void EndMantle();
	bool IsMantling() const;

	

protected:
	virtual void PhysCustom(float deltaTime, int32 Iterations) override;
	virtual void OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement: Sprint", meta = (ClampMin = 0.0f, UIMin = 0.0f))
	float SprintMaxSpeed = 1200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Player Movement: Out of stamina", meta = (ClampMin = 0.0f, UIMin = 0.0f))
	float OutOfStaminaSpeed = 300.0f;

	class ASBaseCharacter* GetBaseCharacterOwner() const;
	
private:
	bool	bIsSprinting;
	bool bIsOutOfStamina = false;

	/*FLedgeDescription TargetLedge;
	FVector InitialMantlingPosition;
	FRotator InitialMantlingRotation;*/
	float TargetMantlingTime = 1.0f;

	FMantlingMovementParameters CurrentMantlingMovementParameters;
	FTimerHandle MantlingTimer;
};

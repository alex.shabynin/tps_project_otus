// Fill out your copyright notice in the Description page of Project Settings.


#include "LedgeDetectorComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Character.h"
#include "DrawDebugHelpers.h"
#include "SGameInstance.h"
#include "TPSDebugSubsystem.h"
#include "TPSTypes.h"
#include "Kismet/GameplayStatics.h"
#include "Utils/TPSTraceUtils.h"


void ULedgeDetectorComponent::BeginPlay()
{
	Super::BeginPlay();
	checkf(GetOwner()->IsA<ACharacter>(), TEXT("ULedgeDetectorComponent::BeginPlay ULedgeDetectorComponent can be used only with ACharacter"));
	CachedCharacterOwner = StaticCast<ACharacter*>(GetOwner());
}

bool ULedgeDetectorComponent::DetectLedge(FLedgeDescription& LedgeDescription)
{
	UCapsuleComponent* CapsuleComponent = CachedCharacterOwner->GetCapsuleComponent(); //Создаем переменную капсулы
	FCollisionQueryParams QueryParams; //Создаем объект структуры 
	QueryParams.bTraceComplex = true; //Включаем определение сложных коллизий
	QueryParams.AddIgnoredActor(GetOwner());//Игнорируем коллизию со своим персонажем

	/*USGameInstance* GameInstance = Cast<USGameInstance> (UGameplayStatics::GetGameInstance(GetWorld()));
	bool bIsDebugEnabled = IsValid(GameInstance) && GameInstance->IsDebugCategoryEnabled(DebugCategoryLedgeDetection);*/
#if	ENABLE_DRAW_DEBUG
	UTPSDebugSubsystem* DebugSubSystem = UGameplayStatics::GetGameInstance(GetWorld())->GetSubsystem<UTPSDebugSubsystem>();
	bool bIsDebugEnabled = DebugSubSystem->IsCategoryEnabled(DebugCategoryLedgeDetection);
#else
	bool bIsDebugEnabled = false;
#endif
	
	float DrawTime = 2.0f;
	float BottomZOffset = 2.0f;
	FVector CharacterBottom = CachedCharacterOwner->GetActorLocation() - (CapsuleComponent->GetScaledCapsuleHalfHeight() - BottomZOffset) * FVector::UpVector;

	

	//Forward check
	float ForwardCheckCapsuleRadius = CapsuleComponent->GetScaledCapsuleRadius();
	float ForwardCheckCapsuleHalfHeight = (MaximumLedgeHeight - MinimumLedgeHeight) * 0.5f;
	
	FHitResult ForwardCheckHitResult;
	//FCollisionShape ForwardCapsuleShape = FCollisionShape::MakeCapsule(ForwardCheckCapsuleRadius, ForwardCheckCapsuleHalfHeight); // Создание капсулы - вызов метода FCollisionShape::MakeCapsule с параметрами радиуса и полуразмера капсулы
	FVector ForwardStartLocation = CharacterBottom + (MinimumLedgeHeight + ForwardCheckCapsuleHalfHeight) * FVector::UpVector; // Стартовая позиция капсулы = ранее определенный CharacterBottom плюс минимальная высота выступа+ForwardCheckCapsuleHalfHeight и преобразование в вектор
	FVector ForwardEndLocation = ForwardStartLocation + CachedCharacterOwner->GetActorForwardVector() * ForwardCheckDistance;
	
	
	if (!TPSTraceUtils::SweepCapsuleSingleByChannel(GetWorld(),ForwardCheckHitResult,ForwardStartLocation,ForwardEndLocation,ForwardCheckCapsuleRadius,ForwardCheckCapsuleHalfHeight,FQuat::Identity,ECC_Climbing,QueryParams,FCollisionResponseParams::DefaultResponseParam,bIsDebugEnabled,DrawTime))
	{
		return false;
	}
	/*DrawDebugCapsule(GetWorld(), ForwardStartLocation, ForwardCheckCapsuleHalfHeight, ForwardCheckCapsuleRadius, FQuat::Identity, FColor::Black, false,DrawTime);
	DrawDebugCapsule(GetWorld(), ForwardEndLocation, ForwardCheckCapsuleHalfHeight, ForwardCheckCapsuleRadius, FQuat::Identity, FColor::Black, false,DrawTime);
	DrawDebugLine(GetWorld(), ForwardStartLocation, ForwardEndLocation, FColor::Black, false, DrawTime);
	if(!GetWorld()->SweepSingleByChannel(ForwardCheckHitResult,ForwardStartLocation,ForwardEndLocation,FQuat::Identity,ECC_Climbing,ForwardCapsuleShape,QueryParams)) //Trace
	{
		return false;
	}
	DrawDebugCapsule(GetWorld(), ForwardCheckHitResult.Location, ForwardCheckCapsuleHalfHeight, ForwardCheckCapsuleRadius, FQuat::Identity, FColor::Red, false,DrawTime);
	DrawDebugPoint(GetWorld(),ForwardCheckHitResult.ImpactPoint, 10.0f, FColor::Red, false, DrawTime);*/
	// Downward check

	FHitResult DownwardCheckHitResult;
	float DownwardSphereCheckRadius = CapsuleComponent->GetScaledCapsuleRadius();
	//FCollisionShape DownwardSphereShape = FCollisionShape::MakeSphere(DownwardSphereCheckRadius);
	float DownwardDepthOffset = 10.0f;
	FVector DownwardStartLocation = ForwardCheckHitResult.ImpactPoint - ForwardCheckHitResult.ImpactNormal * DownwardDepthOffset; //Стартовая позиция сферы = ForwardCheckHitResult точка соприкосновения форвардной капсулы.
	DownwardStartLocation.Z = CharacterBottom.Z + MaximumLedgeHeight + DownwardSphereCheckRadius;
	FVector DownwardEndLocation(DownwardStartLocation.X,DownwardStartLocation.Y,CharacterBottom.Z);

	if (!TPSTraceUtils::SweepSphereSingleByChannel(GetWorld(),DownwardCheckHitResult,DownwardStartLocation,DownwardEndLocation,DownwardSphereCheckRadius,ECC_Climbing,QueryParams,FCollisionResponseParams::DefaultResponseParam,bIsDebugEnabled,DrawTime))
	{
		return false;
	}
	/*FVector DebugCenter = (DownwardStartLocation + DownwardEndLocation) * 0.5f;
	float DebugDrawCapsuleHalfHeight = (DownwardEndLocation - DownwardStartLocation).Size() * 0.5f;

	DrawDebugCapsule(GetWorld(),DebugCenter,DebugDrawCapsuleHalfHeight,DownwardSphereCheckRadius, FQuat::Identity,FColor::Black,false,DrawTime);
	
	if(!GetWorld()->SweepSingleByChannel(DownwardCheckHitResult,DownwardStartLocation,DownwardEndLocation,FQuat::Identity,ECC_Climbing,DownwardSphereShape,QueryParams)) //Trace
		{
		return false;
		}
	DrawDebugSphere(GetWorld(),DownwardCheckHitResult.Location, DownwardSphereCheckRadius, 32, FColor::Red, false,DrawTime);
	DrawDebugPoint(GetWorld(),DownwardCheckHitResult.ImpactPoint, 10.0f, FColor::Red, false, DrawTime);*/
	
	//Overlap check
	float OverlapCapsuleRadius = CapsuleComponent->GetScaledCapsuleRadius();
	float OverlapCapsuleHalfHeight = CapsuleComponent->GetScaledCapsuleHalfHeight();
	
	//FCollisionShape OverlapCapsuleShape = FCollisionShape::MakeCapsule(OverlapCapsuleRadius,OverlapCapsuleHalfHeight);
	float OverlapCapsuleFloorOffset = 2.0f;
	FVector OverlapLocation = DownwardCheckHitResult.ImpactPoint + (OverlapCapsuleHalfHeight + OverlapCapsuleFloorOffset) * FVector::UpVector;

	if(TPSTraceUtils::OverlapCapsuleAnyByProfile(GetWorld(),OverlapLocation, OverlapCapsuleRadius, OverlapCapsuleHalfHeight, FQuat::Identity,CollisionProfilePawn,QueryParams,bIsDebugEnabled,DrawTime))
	{
		return false;
	} 
	
	/*if(GetWorld()->OverlapAnyTestByProfile(OverlapLocation,FQuat::Identity, FName("Pawn"),OverlapCapsuleShape,QueryParams))
	{
		DrawDebugCapsule(GetWorld(),OverlapLocation,OverlapCapsuleHalfHeight,OverlapCapsuleRadius,FQuat::Identity,FColor::Red,false,DrawTime);
		return false;
	}*/

	LedgeDescription.Location = OverlapLocation; //Заполняем структуру LedgeDescription
	LedgeDescription.Rotation = (ForwardCheckHitResult.ImpactNormal * FVector(-1.0f,-1.0f,0.0f)).ToOrientationRotator();
	LedgeDescription.LedgeNormal = ForwardCheckHitResult.ImpactNormal;
	return true;
}




// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AIPatrollingComponent.generated.h"

class ASPatrollingPath;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_PROJECT_API UAIPatrollingComponent : public UActorComponent
{
	GENERATED_BODY()

public:
bool CanPatrol() const;	
FVector SelectClosestWayPoint();
FVector SelectNextWayPoint();	

protected:

	UPROPERTY(EditInstanceOnly,BlueprintReadOnly,Category="Path")
	ASPatrollingPath* PatrollingPath;

private:
	int32 CurrentWayPointIndex = -1;
		
};

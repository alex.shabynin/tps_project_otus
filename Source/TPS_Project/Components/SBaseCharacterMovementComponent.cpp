// Fill out your copyright notice in the Description page of Project Settings.


#include "SBaseCharacterMovementComponent.h"
#include "SBaseCharacter.h"
#include "Curves/CurveVector.h"

float USBaseCharacterMovementComponent::GetMaxSpeed() const
{
	float Result = Super::GetMaxSpeed();
	if (bIsSprinting)
	{
		Result = SprintMaxSpeed;
	}
	else if (bIsOutOfStamina)
	{
		Result = OutOfStaminaSpeed;
	}
	else if(GetBaseCharacterOwner()->IsAiming())
	{
		Result = GetBaseCharacterOwner()->GetAimingMovingSpeed();
	}
	
	return Result;
}

void USBaseCharacterMovementComponent::StartSprint()
{
	bIsSprinting = true;
	bForceMaxAccel = 1;
}

void USBaseCharacterMovementComponent::StopSprint()
{
	bIsSprinting = false;
	bForceMaxAccel = 0;
}

void USBaseCharacterMovementComponent::StartMantle(const FMantlingMovementParameters& MantlingParameters)
{
	CurrentMantlingMovementParameters = MantlingParameters;
	SetMovementMode(MOVE_Custom, uint8(ECustomMovementMode::CMOVE_Mantling));
}

void USBaseCharacterMovementComponent::EndMantle()
{
	SetMovementMode(MOVE_Walking);
}

bool USBaseCharacterMovementComponent::IsMantling() const
{
	return UpdatedComponent && MovementMode == MOVE_Custom && CustomMovementMode == uint8(ECustomMovementMode::CMOVE_Mantling);
}

void USBaseCharacterMovementComponent::PhysCustom(float deltaTime, int32 Iterations)
{
	
	switch(CustomMovementMode)
	{
	case uint8(ECustomMovementMode::CMOVE_Mantling):
		{
			
			float ElapsedTime = GetWorld()->GetTimerManager().GetTimerElapsed(MantlingTimer) + CurrentMantlingMovementParameters.StartTime;

			FVector MantlingCurveValue = CurrentMantlingMovementParameters.MantlingCurve->GetVectorValue(ElapsedTime);

			float PositionAlpha = MantlingCurveValue.X;
			float XYCorrectionAlfa = MantlingCurveValue.Y;
			float ZCorrectionAlpha = MantlingCurveValue.Z;

			FVector CorrectedInitialLocation = FMath::Lerp(CurrentMantlingMovementParameters.InitialLocation, CurrentMantlingMovementParameters.InitialAnimationLocation,XYCorrectionAlfa);
			CorrectedInitialLocation.Z = FMath::Lerp(CurrentMantlingMovementParameters.InitialLocation.Z,CurrentMantlingMovementParameters.InitialAnimationLocation.Z, ZCorrectionAlpha);
			
			FVector NewLocation = FMath::Lerp(CorrectedInitialLocation, CurrentMantlingMovementParameters.TargetLocation, PositionAlpha);
			FRotator NewRotation = 	FMath::Lerp(CurrentMantlingMovementParameters.InitialRotation, CurrentMantlingMovementParameters.TargetRotation, PositionAlpha);

			FVector Delta = NewLocation - GetActorLocation();
			FHitResult Hit; 
			SafeMoveUpdatedComponent(Delta,NewRotation,false,Hit);
			break;
		}
		
	}


	Super::PhysCustom(deltaTime, Iterations);
}

void USBaseCharacterMovementComponent::OnMovementModeChanged(EMovementMode PreviousMovementMode,
                                                             uint8 PreviousCustomMode)
{
	Super::OnMovementModeChanged(PreviousMovementMode, PreviousCustomMode);
	if(MovementMode == MOVE_Custom)
	{
		switch (CustomMovementMode)
		{
			case uint8(ECustomMovementMode::CMOVE_Mantling):
			{
				/*InitialMantlingPosition = GetActorLocation();
				InitialMantlingRotation = GetOwner()->GetActorRotation();
				TargetMantlingTime = 0.25f;*/
				GetWorld()->GetTimerManager().SetTimer(MantlingTimer, this, &USBaseCharacterMovementComponent::EndMantle,CurrentMantlingMovementParameters.Duration, false);
				break;
			}
			default:
				break;
		}
	}
}

ASBaseCharacter* USBaseCharacterMovementComponent::GetBaseCharacterOwner() const
{
	return StaticCast<ASBaseCharacter*>(CharacterOwner);
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
//#include "SAction.h"
#include "Components/ActorComponent.h"
#include "SActionComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnActionStateChange, USActionComponent*, OwningComp, USAction*, Action);
//class USAction;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_PROJECT_API USActionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USActionComponent();

UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Tags")
	FGameplayTagContainer ActivateGameplayTags;

	UFUNCTION(BlueprintCallable,Category="Action")
	void AddAction(AActor* Instigator, TSubclassOf<USAction> ActonClass);
	UFUNCTION(BlueprintCallable,Category="Action")
    bool StartActionByName(AActor* Instigator, FName ActionName);
    UFUNCTION(BlueprintCallable,Category="Action")
    bool StopActionByName(AActor* Instigator, FName ActionName);
	UFUNCTION(BlueprintCallable,Category="Action")
	void RemoveAction(USAction* ActionToRemove);
	UFUNCTION(BlueprintCallable,Category="Action")
	void RemoveAllActions();

	UPROPERTY(BlueprintAssignable)
    FOnActionStateChange OnActionStarted;
    UPROPERTY(BlueprintAssignable)
    FOnActionStateChange OnActionStopped;
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere,Category="Action")
	TArray<TSubclassOf<USAction>> DefaultAction;
	UPROPERTY(BlueprintReadOnly,Category="Action")
	TArray<USAction*> Actions;	

	
};

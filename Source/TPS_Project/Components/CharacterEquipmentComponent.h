// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TPSTypes.h"
#include "CharacterEquipmentComponent.generated.h"

class ASPlayerController;
typedef TArray<class ASEquippableItem*, TInlineAllocator<static_cast<uint32>(EEquipmentSlots::MAX)>> TItemsArray;
typedef TArray<int32, TInlineAllocator<static_cast<uint32>(EAmmunitionType::MAX)>> TAmmunitionArray;

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnCurrentWeaponAmmoChanged,int32, int32);

class ASRangeWeaponItem;
class ASBaseCharacter;
class UEquipmentViewWidget;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_PROJECT_API UCharacterEquipmentComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	EEquippedItemType GetCurrentEquippedItemType() const;

	ASRangeWeaponItem* GetCurrentRangeWeapon() const;

	bool IsEquipping() const;

	UFUNCTION(BlueprintCallable)
	void ReloadCurrentWeapon();

	//void Fire();
	FOnCurrentWeaponAmmoChanged OnCurrentWeaponAmmoChangedEvent;

	void EquipNextItem();
	void EquipPreviousItem();
	void AttachCurrentItemToEquippedSocket();
	void UnequipCurrentItem();

	void EquipItemInSlot(EEquipmentSlots Slot);

	//void AddEquipmentItem(const TSubclassOf<ASEquippableItem> EquippableItemClass);

	bool AddEquipmentItemToSlot(const TSubclassOf<ASEquippableItem> EquipableItemClass, int32 SlotIndex);

	void RemoveItemFromSlot(int32 SlotIndex);

	void OpenViewEquipment(ASPlayerController* PlayerController);

	const TArray<ASEquippableItem*>& GetItems() const;

	void CloseViewEquipment();

	bool IsViewVisible() const;
	
protected:
	/*UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="LoadOut")
	TSubclassOf<ASRangeWeaponItem> SideArmClass;*/

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="LoadOut")
	TMap<EAmmunitionType, int32> MaxAmmunitionAmount;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="LoadOut")
	TMap<EEquipmentSlots, TSubclassOf<class ASEquippableItem>> ItemsLoadout; 

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="LoadOut")
	EEquipmentSlots AutoEquipItemInSlot = EEquipmentSlots::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "View")
	TSubclassOf<UEquipmentViewWidget> ViewWidgetClass;

	void CreateViewWidget(ASPlayerController* PlayerController);
private:
	TAmmunitionArray AmmunitionArray;
	//TItemsArray ItemsArray;
	TArray<ASEquippableItem*> ItemsArray;

	bool bIsEquipping =false;
	
	UFUNCTION()
	void OnWeaponReloadComplete();
	
	void CreateLoadout();

	void AutoEquip();
	
	void EquipAnimationFinished();

	uint32 NextItemArraySlotIndex(uint32 CurrentSlotIndex);
	uint32 PreviousItemArraySlotIndex(uint32 CurrentSlotIndex);
	
	int32 GetAvailableAmmoForCurrentWeapon();
	UFUNCTION()
	void OnCurrentWeaponAmmoChanged(int32 Ammo);

	FDelegateHandle OnCurrentWeaponAmmoChangedHandle;
	FDelegateHandle OnCurrentWeaponReloadedHandle;

	EEquipmentSlots CurrentEquippedSlot;
	ASEquippableItem* CurrentEquippedItem;
	ASRangeWeaponItem* CurrentEquippedWeapon;
	TWeakObjectPtr<ASBaseCharacter> CachedBaseCharacter;
	FTimerHandle EquipTimer;

	UEquipmentViewWidget* ViewWidget;
};

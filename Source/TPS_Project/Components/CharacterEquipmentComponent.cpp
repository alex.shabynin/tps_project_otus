// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterEquipmentComponent.h"

#include "EquipmentViewWidget.h"
#include "SBaseCharacter.h"
#include "SPlayerController.h"
#include "SRangeWeaponItem.h"
#include "TPSTypes.h"


void UCharacterEquipmentComponent::BeginPlay()
{
	Super::BeginPlay();
	checkf(GetOwner()->IsA<ASBaseCharacter>(), TEXT("UCharacterEquipmentComponent::BeginPlay() CharacterEquipmentComponent can be used only with a ASBaseCharacter"));
	CachedBaseCharacter = StaticCast<ASBaseCharacter*>(GetOwner()); //cast to cached Base Character
	CreateLoadout();
	AutoEquip();
}

EEquippedItemType UCharacterEquipmentComponent::GetCurrentEquippedItemType() const
{
	EEquippedItemType Result = EEquippedItemType::None;
	if(IsValid(CurrentEquippedWeapon))
	{
		Result = CurrentEquippedWeapon->GetItemType();
	}
	return Result;
}

ASRangeWeaponItem* UCharacterEquipmentComponent::GetCurrentRangeWeapon() const
{
	return CurrentEquippedWeapon;
}

bool UCharacterEquipmentComponent::IsEquipping() const
{
	return bIsEquipping;
}

void UCharacterEquipmentComponent::ReloadCurrentWeapon()
{
	check(IsValid(CurrentEquippedWeapon));
	int32 AvailableAmmo = GetAvailableAmmoForCurrentWeapon();
	if(AvailableAmmo <=0)
	{
		return;
	}

	CurrentEquippedWeapon->StartReload();
	
	
	/*if(AmmoToReload == CurrentEquippedWeapon->GetMaxAmmo())
	{
		CurrentEquippedWeapon->SetAmmo(ReloadedAmmo);
	}*/
}
void UCharacterEquipmentComponent::UnequipCurrentItem()
{
	if(IsValid(CurrentEquippedItem))
	{
		CurrentEquippedItem->AttachToComponent(CachedBaseCharacter->GetMesh(),FAttachmentTransformRules::KeepRelativeTransform,CurrentEquippedItem->GetUnequippedSocketName());
	}
	if(IsValid(CurrentEquippedWeapon))
	{
		CurrentEquippedWeapon->StopFire();
		CurrentEquippedWeapon->EndReload(false);
		CurrentEquippedWeapon->OnAmmoChanged.Remove(OnCurrentWeaponAmmoChangedHandle);
		CurrentEquippedWeapon->OnReloadComplete.Remove(OnCurrentWeaponReloadedHandle);
	}
	CurrentEquippedSlot = EEquipmentSlots::None;

}

void UCharacterEquipmentComponent::EquipNextItem()
{
	uint32 CurrentSlotIndex = (uint32)CurrentEquippedSlot;
	uint32 NextSlotIndex = NextItemArraySlotIndex(CurrentSlotIndex);
	
	while(CurrentSlotIndex !=NextSlotIndex && !IsValid(ItemsArray[NextSlotIndex]))
	{
		NextSlotIndex = NextItemArraySlotIndex(NextSlotIndex);
	}
	if(CurrentSlotIndex !=NextSlotIndex)
	{
		EquipItemInSlot((EEquipmentSlots)NextSlotIndex);
	}
}

void UCharacterEquipmentComponent::EquipPreviousItem()
{
	uint32 CurrentSlotIndex = (uint32)CurrentEquippedSlot;
	uint32 PreviousSlotIndex = PreviousItemArraySlotIndex(CurrentSlotIndex);
	
	while(CurrentSlotIndex !=PreviousSlotIndex && !IsValid(ItemsArray[PreviousSlotIndex]))
	{
		PreviousSlotIndex = PreviousItemArraySlotIndex(PreviousSlotIndex);
	}
	if(CurrentSlotIndex !=PreviousSlotIndex)
	{
		EquipItemInSlot((EEquipmentSlots)PreviousSlotIndex);
	}
}


void UCharacterEquipmentComponent::AttachCurrentItemToEquippedSocket()
{
	CurrentEquippedItem->AttachToComponent(CachedBaseCharacter->GetMesh(),FAttachmentTransformRules::KeepRelativeTransform,CurrentEquippedItem->GetEquippedSocketName());
}

void UCharacterEquipmentComponent::EquipItemInSlot(EEquipmentSlots Slot)
{
	if(bIsEquipping)
	{
		return;
	}
	UnequipCurrentItem();
	CurrentEquippedItem = ItemsArray[(uint32)Slot];
	CurrentEquippedWeapon = Cast<ASRangeWeaponItem>(CurrentEquippedItem);

	if(IsValid(CurrentEquippedItem))
	{
		UAnimMontage* EquipMontage = CurrentEquippedItem->GetCharacterEquipAnimMontage();
		if(IsValid(EquipMontage))
		{
			bIsEquipping = true;
			float EquipDuration = CachedBaseCharacter->PlayAnimMontage(EquipMontage);
			GetWorld()->GetTimerManager().SetTimer(EquipTimer,this,&UCharacterEquipmentComponent::EquipAnimationFinished,EquipDuration,false);
		}
		else
		{
			AttachCurrentItemToEquippedSocket();
		}
		CurrentEquippedSlot = Slot;
		
	}
	/*else
	{
		CurrentEquippedSlot = EEquipmentSlots::None;
	}*/
	if(IsValid(CurrentEquippedWeapon))
    {
    	OnCurrentWeaponAmmoChangedHandle = CurrentEquippedWeapon->OnAmmoChanged.AddUFunction(this,FName("OnCurrentWeaponAmmoChanged"));
    	OnCurrentWeaponReloadedHandle = CurrentEquippedWeapon->OnReloadComplete.AddUFunction(this,FName("OnWeaponReloadComplete"));
    	OnCurrentWeaponAmmoChanged(CurrentEquippedWeapon->GetAmmo());
    }
}

/*void UCharacterEquipmentComponent::AddEquipmentItem(const TSubclassOf<ASEquippableItem> EquippableItemClass)
{
	ASRangeWeaponItem* RangeWeaponObject = Cast<ASRangeWeaponItem>(EquippableItemClass->GetDefaultObject());
	if(!IsValid(RangeWeaponObject))
	{
		return;
	}

	AmmunitionArray[(uint32)RangeWeaponObject->GetAmmoType()] += RangeWeaponObject->GetMaxAmmo();
	if(IsValid(CurrentEquippedWeapon))
	{
		OnCurrentWeaponAmmoChanged(CurrentEquippedWeapon->GetAmmo());
	}
}*/

bool UCharacterEquipmentComponent::AddEquipmentItemToSlot(const TSubclassOf<ASEquippableItem> EquipableItemClass,
	int32 SlotIndex)
{
	if (!IsValid(EquipableItemClass))
	{
		return false;
	}
	ASEquippableItem* DefaultItemObject = EquipableItemClass->GetDefaultObject<ASEquippableItem>();
	if (!DefaultItemObject->IsSlotCompatible((EEquipmentSlots)SlotIndex))
	{
		return false;
	}
	if (!IsValid(ItemsArray[SlotIndex]))
	{
		ASEquippableItem* Item = GetWorld()->SpawnActor<ASEquippableItem>(EquipableItemClass);
		Item->AttachToComponent(CachedBaseCharacter->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, Item->GetUnequippedSocketName());
		Item->SetOwner(CachedBaseCharacter.Get());
		Item->UnEquip();
		ItemsArray[SlotIndex] = Item;
	}
	else if (DefaultItemObject->IsA<ASRangeWeaponItem>())
	{
		ASRangeWeaponItem* RangeWeaponObject = StaticCast<ASRangeWeaponItem*>(DefaultItemObject);
		int32 AmmoSlotIndex = (int32)RangeWeaponObject->GetAmmoType();
		AmmunitionArray[SlotIndex] += RangeWeaponObject->GetMaxAmmo();
	}

	return true;
}

void UCharacterEquipmentComponent::RemoveItemFromSlot(int32 SlotIndex)
{
	if ((uint32)CurrentEquippedSlot == SlotIndex)
	{
		UnequipCurrentItem();
	}
	ItemsArray[SlotIndex]->Destroy();
	ItemsArray[SlotIndex] = nullptr;
}

void UCharacterEquipmentComponent::OpenViewEquipment(ASPlayerController* PlayerController)
{
	if (!IsValid(ViewWidget))
	{
		CreateViewWidget(PlayerController);
	}

	if (!ViewWidget->IsVisible())
	{
		ViewWidget->AddToViewport();
	}
}

const TArray<ASEquippableItem*>& UCharacterEquipmentComponent::GetItems() const
{
	return ItemsArray;
}

void UCharacterEquipmentComponent::CloseViewEquipment()
{
	if (ViewWidget->IsVisible())
	{
		ViewWidget->RemoveFromParent();
	}
}

bool UCharacterEquipmentComponent::IsViewVisible() const
{
	bool Result = false;
	if(IsValid(ViewWidget))
	{
		Result = ViewWidget->IsVisible();
	}
	return Result;
}

/*void UCharacterEquipmentComponent::Fire()
{
	if(IsValid(CurrentEquippedWeapon))
	{
		CurrentEquippedWeapon->Fire();
	}
}*/



void UCharacterEquipmentComponent::CreateLoadout()
{
	AmmunitionArray.AddZeroed((uint32)EAmmunitionType::MAX);
	for(const TPair<EAmmunitionType, int32>& AmmoPair : MaxAmmunitionAmount)
	{
		AmmunitionArray[(uint32)AmmoPair.Key] = FMath::Max(AmmoPair.Value, 0);
	}

	ItemsArray.AddZeroed((uint32)EEquipmentSlots::MAX);
	for(const TPair<EEquipmentSlots, TSubclassOf<ASEquippableItem>>& ItemPair : ItemsLoadout)
	{
		if(!IsValid(ItemPair.Value))
		{
			continue;
		}
		ASEquippableItem* Item = GetWorld()->SpawnActor<ASEquippableItem>(ItemPair.Value);
		Item->AttachToComponent(CachedBaseCharacter->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, Item->GetUnequippedSocketName());
		Item->SetOwner(CachedBaseCharacter.Get());
		ItemsArray[(uint32)ItemPair.Key] = Item;

		AddEquipmentItemToSlot(ItemPair.Value, (int32)ItemPair.Key);
	}
	
	/*if(!IsValid(SideArmClass))
	{
		return;
	}*/
	
		/*CurrentEquippedWeapon = GetWorld()->SpawnActor<ASRangeWeaponItem>(SideArmClass);
		CurrentEquippedWeapon->AttachToComponent(CachedBaseCharacter->GetMesh(),FAttachmentTransformRules::KeepRelativeTransform,SocketCharacterWeapon);
		CurrentEquippedWeapon->SetOwner(CachedBaseCharacter.Get());
		CurrentEquippedWeapon->OnAmmoChanged.AddUFunction(this,FName("OnCurrentWeaponAmmoChanged"));
		CurrentEquippedWeapon->OnReloadComplete.AddUFunction(this,FName("OnWeaponReloadComplete"));
		OnCurrentWeaponAmmoChanged(CurrentEquippedWeapon->GetAmmo());*/

		
}

void UCharacterEquipmentComponent::AutoEquip()
{
	if(AutoEquipItemInSlot != EEquipmentSlots::None)
	{
		EquipItemInSlot(AutoEquipItemInSlot);
	}
}

void UCharacterEquipmentComponent::EquipAnimationFinished()
{
	bIsEquipping = false;
	AttachCurrentItemToEquippedSocket();
}

uint32 UCharacterEquipmentComponent::NextItemArraySlotIndex(uint32 CurrentSlotIndex)
{
	if(CurrentSlotIndex == ItemsArray.Num() - 1)
	{
		return 0;
	}
	else
	{
		return CurrentSlotIndex +1;
	}
}

uint32 UCharacterEquipmentComponent::PreviousItemArraySlotIndex(uint32 CurrentSlotIndex)
{
	if(CurrentSlotIndex == 0)
	{
		return ItemsArray.Num() - 1;
	}
	else
	{
		return CurrentSlotIndex - 1;
	}
}

int32 UCharacterEquipmentComponent::GetAvailableAmmoForCurrentWeapon()
{
	check(GetCurrentRangeWeapon());
	return AmmunitionArray[(uint32)GetCurrentRangeWeapon()->GetAmmoType()];
}

void UCharacterEquipmentComponent::CreateViewWidget(ASPlayerController* PlayerController)
{
	checkf(IsValid(ViewWidgetClass), TEXT("UCharacterEquipmentComponent::CreateViewWidget view widget class is not defined"));

	if (!IsValid(PlayerController))
	{
		return;
	}

	ViewWidget = CreateWidget<UEquipmentViewWidget>(PlayerController, ViewWidgetClass);
	ViewWidget->InitializeEquipmentWidget(this);
}

void UCharacterEquipmentComponent::OnWeaponReloadComplete()
{
	int32 AvailableAmmo = GetAvailableAmmoForCurrentWeapon();
	int32 CurrentAmmo = CurrentEquippedWeapon->GetAmmo();
	int32 AmmoToReload = CurrentEquippedWeapon->GetMaxAmmo() - CurrentAmmo;
	int32 ReloadedAmmo = FMath::Min(AvailableAmmo,AmmoToReload);
	AmmunitionArray[(uint32)CurrentEquippedWeapon->GetAmmoType()] -= ReloadedAmmo;
	CurrentEquippedWeapon->SetAmmo(ReloadedAmmo + CurrentAmmo);
}
void UCharacterEquipmentComponent::OnCurrentWeaponAmmoChanged(int32 Ammo)
{
	if(OnCurrentWeaponAmmoChangedEvent.IsBound())
	{
		OnCurrentWeaponAmmoChangedEvent.Broadcast(Ammo, GetAvailableAmmoForCurrentWeapon());
	}
}

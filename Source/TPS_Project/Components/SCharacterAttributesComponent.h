// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SCharacterAttributesComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnDeathEvent);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnHealthChanged, float);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_PROJECT_API USCharacterAttributesComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	USCharacterAttributesComponent();
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FOnDeathEvent OnDeathEvent;
	FOnHealthChanged OnHealthChangedEvent;
	bool IsAlive() {return Health > 0;}

	float GetHealthPercent() const;

	void AddHealth(float HealthToAdd);
	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= "Health", meta=(UIMin=0.0f))
	float MaxHealth = 100.0f;

public:
	UFUNCTION(BlueprintCallable)
	float GetHealth() const;
	UFUNCTION(BlueprintCallable,Category="Attributes")
	bool ApplyDamage(float Delta);


private:
	float Health = 0.0f;

	void OnHealthChanged();
	
#if UE_BUILD_DEBUG || UE_BUILD_DEVELOPMENT
	void DebugDrawAttributes();
#endif

	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	TWeakObjectPtr<class ASBaseCharacter> CachedBaseCharacterOwner;
};

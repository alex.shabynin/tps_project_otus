// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/SActionComponent.h"
#include "Actors/Actions/SAction.h"

USActionComponent::USActionComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

}
void USActionComponent::BeginPlay()
{
	Super::BeginPlay();

	for (TSubclassOf<USAction> ActionClass : DefaultAction)
	{
		AddAction(GetOwner(),ActionClass);
	}
	
}

void USActionComponent::AddAction(AActor* Instigator, TSubclassOf<USAction> ActionClass)
{
	if(!IsValid(ActionClass))
	{
		return;
	}

	USAction* NewAction = NewObject<USAction>(this,ActionClass);
	if(IsValid(NewAction))
	{
		Actions.Add(NewAction);

		if(NewAction->bAutoActivate)
		{
			NewAction->StartAction(Instigator);
		}
	}
}

bool USActionComponent::StartActionByName(AActor* Instigator, FName ActionName)
{
	for (USAction* Action : Actions)
	{
		if(Action && Action->ActionName == ActionName)
		{
			if(!Action->CanStart(Instigator))
			{
				UE_LOG(LogTemp, Warning, TEXT("Failed ti run Action: %s"), *ActionName.ToString());
				continue;
			}
			Action->StartAction(Instigator);
		}	return true;
	}
	return false;
}

bool USActionComponent::StopActionByName(AActor* Instigator, FName ActionName)
{
	for (USAction* Action : Actions)
	{
		if(Action && Action->ActionName == ActionName)
		{
			if(Action->IsRunning())
			{
				Action->StopAction(Instigator);
				return true;
			}
		}
	}
	return false;
}

void USActionComponent::RemoveAction(USAction* ActionToRemove)
{
	if(!IsValid(ActionToRemove) && !ActionToRemove->IsRunning())
	{
		return;
	}
	Actions.Remove(ActionToRemove);
}

void USActionComponent::RemoveAllActions()
{
	if(Actions.Num() > 0)
	{
		for (int i =0; i <=Actions.Num() - 1; ++i)
		{
			RemoveAction(Actions[i]);
		}
		UE_LOG(LogTemp, Warning, TEXT("All Actions removed"));
	}
}







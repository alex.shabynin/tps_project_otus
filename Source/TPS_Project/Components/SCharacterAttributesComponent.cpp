// Fill out your copyright notice in the Description page of Project Settings.


#include "SCharacterAttributesComponent.h"

#include "SBaseCharacter.h"
#include "TPSDebugSubsystem.h"
#include "TPSTypes.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
USCharacterAttributesComponent::USCharacterAttributesComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

float USCharacterAttributesComponent::GetHealthPercent() const
{

	return Health / MaxHealth;
}

void USCharacterAttributesComponent::AddHealth(float HealthToAdd)
{
	Health = FMath::Clamp(Health + HealthToAdd, 0.0f, MaxHealth);
	OnHealthChanged();
}

void USCharacterAttributesComponent::BeginPlay()
{
	Super::BeginPlay();

	checkf(MaxHealth > 0.0f, TEXT("USCharacterAttributesComponent::BeginPlay() max health must be greater than zero"));
	checkf(GetOwner()->IsA<ASBaseCharacter>(), TEXT("USCharacterAttributesComponent::BeginPlay USCharacterAttributesComponent can be used only with ASBaseCharacter"));
	CachedBaseCharacterOwner = StaticCast<ASBaseCharacter*>(GetOwner());

	Health = MaxHealth;
	CachedBaseCharacterOwner->OnTakeAnyDamage.AddDynamic(this, &USCharacterAttributesComponent::OnTakeAnyDamage);
}

void USCharacterAttributesComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
#if UE_BUILD_DEBUG || UE_BUILD_DEVELOPMENT
	DebugDrawAttributes();
#endif

}
void USCharacterAttributesComponent::OnHealthChanged()
{
	if (OnHealthChangedEvent.IsBound())
	{
		OnHealthChangedEvent.Broadcast(GetHealthPercent());
	}
	if (Health <= 0.0f)
	{
		if (OnDeathEvent.IsBound())
		{
			OnDeathEvent.Broadcast();
		}
	}
}
#if UE_BUILD_DEBUG || UE_BUILD_DEVELOPMENT
void USCharacterAttributesComponent::DebugDrawAttributes()
{
	UTPSDebugSubsystem* DebugSubsystem = UGameplayStatics::GetGameInstance(GetWorld())->GetSubsystem<UTPSDebugSubsystem>();
	if(!DebugSubsystem->IsCategoryEnabled(DebugCategoryCharacterAttributes))
	{
		return;
	}
	FVector TextLocation = CachedBaseCharacterOwner->GetActorLocation() + (CachedBaseCharacterOwner->GetCapsuleComponent()->GetScaledCapsuleHalfHeight() +5.0f) * FVector::UpVector;
	DrawDebugString(GetWorld(),TextLocation,FString::Printf (TEXT("Health %.2f"),Health), nullptr,FColor::Green, 0.0f, true);
}
#endif

void USCharacterAttributesComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
                                                     AController* InstigatedBy, AActor* DamageCauser)
{
	if(!IsAlive())
	{
		return;
	}

	UE_LOG(LogDamage, Warning, TEXT("USCharacterAttributesComponent::OnTakeAnyDamage %s received %.2f amount of damage from %s"), *CachedBaseCharacterOwner->GetName(), Damage, *DamageCauser->GetName());
	Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);
	OnHealthChanged();

	/*if(Health <= 0.0f)
	{
		UE_LOG(LogDamage, Warning, TEXT("USCharacterAttributesComponent::OnTakeAnyDamage character %s has been killed by actor %s"), *CachedBaseCharacterOwner->GetName(), *DamageCauser->GetName());
		if(OnDeathEvent.IsBound())
		{
			OnDeathEvent.Broadcast();
		}
	}*/
}

float USCharacterAttributesComponent::GetHealth() const

{
	
	return Health;
	
}

bool USCharacterAttributesComponent::ApplyDamage(float Delta)
{

	if(!IsAlive())
	{
		return false;
	}
	Health += Delta;
	
	if(Health <= 0.0f)
	{
		//UE_LOG(LogDamage, Warning, TEXT("USCharacterAttributesComponent::OnTakeAnyDamage character %s has been killed by actor %s"), *CachedBaseCharacterOwner->GetName(), *DamageCauser->GetName());
		if(OnDeathEvent.IsBound())
		{
			OnDeathEvent.Broadcast();
		}
	}
	return true;
}

// Called every frame





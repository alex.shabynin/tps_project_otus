// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSDebugSubsystem.h"

bool UTPSDebugSubsystem::IsCategoryEnabled(const FName& CategoryName) const
{
	const bool* bIsEnabled = EnabledDebugCategories.Find(CategoryName);
	return bIsEnabled != nullptr && *bIsEnabled;	
}

void UTPSDebugSubsystem::EnableDebugCategory(const FName& CategoryName, bool bIsEnabled)
{
	EnabledDebugCategories.FindOrAdd(CategoryName);
	EnabledDebugCategories[CategoryName] = bIsEnabled;
}

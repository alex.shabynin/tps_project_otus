// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SInventoryItem.h"
#include "WeaponInventoryItem.generated.h"

/**
 * 
 */
class ASEquippableItem;
UCLASS()
class TPS_PROJECT_API UWeaponInventoryItem : public USInventoryItem
{
	GENERATED_BODY()
public:
	UWeaponInventoryItem();

	void SetEquipWeaponClass(TSubclassOf<ASEquippableItem>& WeaponClass);
	TSubclassOf<ASEquippableItem> GetEquipWeaponClass() const;

protected:
	TSubclassOf<ASEquippableItem> EquipWeaponClass;
};


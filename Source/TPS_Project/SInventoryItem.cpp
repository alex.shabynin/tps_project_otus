// Fill out your copyright notice in the Description page of Project Settings.


#include "SInventoryItem.h"

void USInventoryItem::Initialize(FName DataTableID_In, const FInventoryItemDescription& Description_In)
{
	DataTableID = DataTableID_In;
	Description.Icon = Description_In.Icon;
	Description.Name = Description_In.Name;
	bIsInitialized = true;
}

FName USInventoryItem::GetDataTableID() const
{
	return DataTableID;
}

const FInventoryItemDescription& USInventoryItem::GetDescription() const
{
	return Description;
}

bool USInventoryItem::IsEquippable() const
{
	return bIsEquippable;
}

bool USInventoryItem::IsConsumable() const
{
	return bIsConsumable;
}





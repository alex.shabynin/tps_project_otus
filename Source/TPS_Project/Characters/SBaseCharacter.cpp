// Fill out your copyright notice in the Description page of Project Settings.


#include "SBaseCharacter.h"

#include "AIController.h"
#include "CharacterEquipmentComponent.h"
#include "Interactive.h"
#include "LedgeDetectorComponent.h"
#include "SActionComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "SBaseCharacterMovementComponent.h"
#include "SCharacterAttributesComponent.h"
#include "SCharacterInventoryComponent.h"
#include "SPlayerController.h"
#include "SRangeWeaponItem.h"
#include "TPSTypes.h"
#include "Curves/CurveVector.h"
#include "Engine/DamageEvents.h"


void ASBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	CharacterAttributesComponent->OnDeathEvent.AddUObject(this, &ASBaseCharacter::OnDeath);
}

void ASBaseCharacter::EndPlay(const EEndPlayReason::Type Reason)
{
	if (OnInteractableObjectFound.IsBound())
	{
		OnInteractableObjectFound.Unbind();
	}
	Super::EndPlay(Reason);
}

void ASBaseCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	AAIController* AIController = Cast<AAIController>(NewController);
	if(IsValid(AIController))
	{
		FGenericTeamId TeamId((uint8)Team);
		AIController->SetGenericTeamId(TeamId);
	}
}

ASBaseCharacter::ASBaseCharacter(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer.SetDefaultSubobjectClass<USBaseCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	SBaseCharacterMovementComponent = StaticCast<USBaseCharacterMovementComponent*>(GetCharacterMovement());
	CharacterAttributesComponent = CreateDefaultSubobject<USCharacterAttributesComponent>(TEXT("CharacterAttributesComponent"));
	LedgeDetectorComponent = CreateDefaultSubobject<ULedgeDetectorComponent>(TEXT("LedgeDetectorComponent"));
	CharacterEquipmentComponent = CreateDefaultSubobject<UCharacterEquipmentComponent>(TEXT("CharacterEquipmentComponent"));
	CharacterActionComponent = CreateDefaultSubobject<USActionComponent>(TEXT("CharacterActionComponent"));
	CharacterInventoryComponent = CreateDefaultSubobject<USCharacterInventoryComponent>(TEXT("CharacterInventoryComponent"));

	CurrentStamina = MaxStamina;
}

void ASBaseCharacter::ChangeCrouchState()
{
	if (GetCharacterMovement()->IsCrouching())
	{
		UnCrouch();
	}
	else
	{
		Crouch();
	}
}

void ASBaseCharacter::StartSprint()
{
	bIsSprintRequested = true;
	if (bIsCrouched)
	{
		UnCrouch();
	}
}

void ASBaseCharacter::StopSprint()
{
	bIsSprintRequested = false;
}

void ASBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TryChangeSprintState(DeltaTime);
	UpdateStamina(DeltaTime);
	//DebugDrawStamina();
	TraceLineOfSight();
}

void ASBaseCharacter::Mantle()
{
	if(!CanMantle())
	{
		return;
	}
	FLedgeDescription LedgeDescription;
	if(LedgeDetectorComponent->DetectLedge(LedgeDescription) && !GetBaseCharacterMovementComponent()->IsMantling() && !GetCharacterMovement()->IsCrouching())
	{
		FMantlingMovementParameters MantlingParameters;
		MantlingParameters.InitialLocation = GetActorLocation();
		MantlingParameters.InitialRotation = GetActorRotation();
		MantlingParameters.TargetLocation = LedgeDescription.Location;
		MantlingParameters.TargetRotation = LedgeDescription.Rotation;

		float MantlingHeight = (MantlingParameters.TargetLocation - MantlingParameters.InitialLocation).Z;
		const FMantlingSettings& MantlingSettings = GetMantlingSettings(MantlingHeight);

		float MinRange;
		float MaxRange;
		MantlingSettings.MantlingCurve->GetTimeRange(MinRange,MaxRange);
		MantlingParameters.Duration = MaxRange - MinRange;


		MantlingParameters.MantlingCurve = MantlingSettings.MantlingCurve;


		FVector2D SourceRange(MantlingSettings.MinHeight, MantlingSettings.MaxHeight);
		FVector2D TargetRange(MantlingSettings.MinHeightStartTime, MantlingSettings.MaxHeightStartTime);
		MantlingParameters.StartTime = FMath::GetMappedRangeValueClamped(SourceRange,TargetRange,MantlingHeight);

		MantlingParameters.InitialAnimationLocation = MantlingParameters.TargetLocation - MantlingSettings.AnimationCorrectionZ * FVector::UpVector + MantlingSettings.AnimationCorrectionXY * LedgeDescription.LedgeNormal;

		GetBaseCharacterMovementComponent()->StartMantle(MantlingParameters);

		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		AnimInstance->Montage_Play(MantlingSettings.MantlingMontage, 1.0f, EMontagePlayReturnType::Duration, MantlingParameters.StartTime);
	}
}

void ASBaseCharacter::StartFire()
{
	if(CharacterEquipmentComponent->IsEquipping())
	{
		return;
	}
	ASRangeWeaponItem* CurrentRangeWeapon = CharacterEquipmentComponent->GetCurrentRangeWeapon();
	if(IsValid(CurrentRangeWeapon))
	{
		CurrentRangeWeapon->StartFire();
	}
}

void ASBaseCharacter::StopFire()
{
	ASRangeWeaponItem* CurrentRangeWeapon = CharacterEquipmentComponent->GetCurrentRangeWeapon();
	if(IsValid(CurrentRangeWeapon))
	{
		CurrentRangeWeapon->StopFire();
	}
}

void ASBaseCharacter::StartAim()
{
	ASRangeWeaponItem* CurrentRangeWeapon = GetCharacterEquipmentComponent()->GetCurrentRangeWeapon();
	if(!IsValid(CurrentRangeWeapon))
	{
		return;
	}
	bIsAiming = true;
	CurrentAimingMovingSpeed = CurrentRangeWeapon->GetAimMovingSpeed();
	CurrentRangeWeapon->StartAim();
	OnStartAiming();
}

void ASBaseCharacter::StopAim()
{
	if(!bIsAiming)
	{
		return;
	}

	ASRangeWeaponItem* CurrentRangeWeapon = GetCharacterEquipmentComponent()->GetCurrentRangeWeapon();
	if(IsValid(CurrentRangeWeapon))
	{
		CurrentRangeWeapon->StopAim();
	}
	
	bIsAiming = false;
	CurrentAimingMovingSpeed = 0.0f;
	OnStopAiming();
}

void ASBaseCharacter::Reload()
{
	if(IsValid(CharacterEquipmentComponent->GetCurrentRangeWeapon()))
	{
		CharacterEquipmentComponent->ReloadCurrentWeapon();
	}
}

void ASBaseCharacter::NextItem()
{
	CharacterEquipmentComponent->EquipNextItem();
}

void ASBaseCharacter::PreviousItem()
{
	CharacterEquipmentComponent->EquipPreviousItem();
}

void ASBaseCharacter::OnStartAiming_Implementation()
{
	OnStartAimingInternal();
}

void ASBaseCharacter::OnStopAiming_Implementation()
{
	OnStopAimingInternal();
}

float ASBaseCharacter::GetAimingMovingSpeed() const
{
	return CurrentAimingMovingSpeed;
}

bool ASBaseCharacter::IsAiming() const
{
	return bIsAiming;
}

bool ASBaseCharacter::CanJumpInternal_Implementation() const
{
	return Super::CanJumpInternal_Implementation() && !GetBaseCharacterMovementComponent()->IsMantling();
}

void ASBaseCharacter::Falling()
{
	Super::Falling();
	GetBaseCharacterMovementComponent()->bNotifyApex = true;
}

void ASBaseCharacter::NotifyJumpApex()
{
	Super::NotifyJumpApex();
	CurrentFallApex = GetActorLocation();
}

void ASBaseCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);
	float FallHeight = (CurrentFallApex - GetActorLocation()).Z * 0.01f;
	if(IsValid(FallDamageCurve))
	{
		float DamageAmount = FallDamageCurve->GetFloatValue((FallHeight));
		TakeDamage(DamageAmount,FDamageEvent(),GetController(),Hit.GetActor());
	}
}

float ASBaseCharacter::GetStaminaPercent()
{
	return CurrentStamina / MaxStamina;
}

float ASBaseCharacter::GetStamina()
{
	return CurrentStamina;
}

/*float ASBaseCharacter::GetHealthPercent() const
{
	return CharacterAttributesComponent->GetHealthPercent();
}*/

UCharacterEquipmentComponent* ASBaseCharacter::GetCharacterEquipmentComponent_Mutable() const
{
	return CharacterEquipmentComponent;
}

const UCharacterEquipmentComponent* ASBaseCharacter::GetCharacterEquipmentComponent() const
{
	return CharacterEquipmentComponent;
}

USCharacterAttributesComponent* ASBaseCharacter::GetCharacterAttributesComponent() const
{
	return CharacterAttributesComponent;
}

const USActionComponent* ASBaseCharacter::GetCharacterActionComponent() const
{
	return CharacterActionComponent;
}

FGenericTeamId ASBaseCharacter::GetGenericTeamId() const
{
	return FGenericTeamId((uint8)Team);
}

void ASBaseCharacter::Interact()
{
	if(LineOfSightObject.GetInterface())
	{
		LineOfSightObject->Interact(this);
	}
}

/*void ASBaseCharacter::AddEquipmentItem(const TSubclassOf<ASEquippableItem> EquippableItemClass)
{
	CharacterEquipmentComponent->AddEquipmentItem(EquippableItemClass);

}*/

bool ASBaseCharacter::PickupItem(TWeakObjectPtr<USInventoryItem> ItemToPickup)
{
	bool Result = false;
	if(CharacterInventoryComponent->HasFreeSlot())
	{
		CharacterInventoryComponent->AddItem(ItemToPickup,1);
		Result = true;
	}
	return Result;
}

void ASBaseCharacter::UseInventory(ASPlayerController* PlayerController)
{
	if (!IsValid(PlayerController))
	{
		return;
	}
	if (!CharacterInventoryComponent->IsViewVisible())
	{
		CharacterInventoryComponent->OpenViewInventory(PlayerController);
		CharacterEquipmentComponent->OpenViewEquipment(PlayerController);
		PlayerController->SetInputMode(FInputModeGameAndUI{});
		PlayerController->bShowMouseCursor = true;
	}
	else
	{
		CharacterInventoryComponent->CloseViewInventory();
		CharacterEquipmentComponent->CloseViewEquipment();
		PlayerController->SetInputMode(FInputModeGameOnly{});
		PlayerController->bShowMouseCursor = false;
	}
}

void ASBaseCharacter::RestoreFullStamina()
{
	CurrentStamina = MaxStamina;
}


/*float ASBaseCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	/*UE_LOG(LogDamage, Warning, TEXT("ASBaseCharacter::TakeDamage %s received %.2f amount of damage from %s"), *GetName(), DamageAmount, *DamageCauser->GetName())#1#
	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}*/

bool ASBaseCharacter::CanSprint()
{
	return !(SBaseCharacterMovementComponent->IsOutOfStamina() || SBaseCharacterMovementComponent->IsCrouching());
}


void ASBaseCharacter::TraceLineOfSight()
{
	if(!IsPlayerControlled())
	{
		return;
	}
	FVector ViewLocation;
	FRotator ViewRotation;
	APlayerController* PlayerController = GetController<APlayerController>();
	PlayerController->GetPlayerViewPoint(ViewLocation,ViewRotation);

	FVector ViewDirection = ViewRotation.Vector();
	FVector TraceEnd = ViewLocation + ViewDirection * LineOfSightDistance;

	FHitResult HitResult;
	GetWorld()->LineTraceSingleByChannel(HitResult,ViewLocation,TraceEnd,ECC_Visibility);
		if (LineOfSightObject.GetObject() !=HitResult.GetActor())
		{
			LineOfSightObject = HitResult.GetActor();

			FName ActionName;
			if(LineOfSightObject.GetInterface())
			{
				ActionName = LineOfSightObject->GetActionEventName();
			}
			else
			{
				ActionName = NAME_None;
			}
			OnInteractableObjectFound.ExecuteIfBound(ActionName);
		}
}

void ASBaseCharacter::OnStartAimingInternal()
{
	if(OnAimingStateChanged.IsBound())
	{
		OnAimingStateChanged.Broadcast(true);
	}
}

void ASBaseCharacter::OnStopAimingInternal()
{
	if(OnAimingStateChanged.IsBound())
	{
		OnAimingStateChanged.Broadcast(false);
	}
}

void ASBaseCharacter::TryChangeSprintState(float DeltaTime)
{
if (bIsSprintRequested && !SBaseCharacterMovementComponent->IsSprinting() && CanSprint())
	{
		/*bIsSprinting = true;
		DefaultMaxMovSpeed = GetCharacterMovement()->MaxWalkSpeed;
		GetCharacterMovement()->MaxWalkSpeed = SprintMaxSpeed;
		GetCharacterMovement()->bForceMaxAccel = 1;*/
		SBaseCharacterMovementComponent->StartSprint();
		
		
	}
	if (SBaseCharacterMovementComponent->IsSprinting())
	{
		CurrentStamina -= SprintStaminaConsumptionVelocity * DeltaTime;
		CurrentStamina = FMath::Clamp(CurrentStamina, 0.0f, MaxStamina);
	}
	if (SBaseCharacterMovementComponent->IsSprinting() && CurrentStamina == 0.0f)
	{
		SBaseCharacterMovementComponent->StopSprint();
	}
	//Just Testing
	if(SBaseCharacterMovementComponent->IsSprinting() && CurrentStamina <= 5.0f)
	{
		OutOfStamina();
	}

	if (!bIsSprintRequested && SBaseCharacterMovementComponent->IsSprinting())
	{
		/*bIsSprinting = false;
		GetCharacterMovement()->MaxWalkSpeed = DefaultMaxMovSpeed;
		GetCharacterMovement()->bForceMaxAccel = 0;*/
		SBaseCharacterMovementComponent->StopSprint();
	}
}

void ASBaseCharacter::DebugDrawStamina()
{
	if (CurrentStamina < MaxStamina)
	{
		GEngine->AddOnScreenDebugMessage(1, 1.0f, FColor::Yellow, FString::Printf(TEXT("Stamina: %.2f"), CurrentStamina));
	}
}

void ASBaseCharacter::OnDeath()
{
	GetCharacterMovement()->DisableMovement();
	float Duration = PlayAnimMontage(OnDeathAnimMontage);
	
	if(Duration == 0.0f)
	/*{
		GetWorld()->GetTimerManager().SetTimer(DeathMontageTimer, this, &ASBaseCharacter::EnableRagdoll, Duration,false);
	}
	else#1#*/
	{
		EnableRagdoll();
	}


	/*GetCharacterMovement()->DisableMovement();
	float Duration = PlayAnimMontage(OnDeathAnimMontage);
	if(Duration == 0)
	{
		EnableRagdoll();
	}

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(AnimInstance && OnDeathAnimMontage)
	{
		AnimInstance->Montage_Play(OnDeathAnimMontage);

		const int32 Selection = FMath::RandRange(0,3);
		FName SectionName = FName();
		switch(Selection)
		{
		case 0:
			SectionName = FName("Death1");
			break;
		case 1:
			SectionName = FName("Death2");
			break;
		case 2:
			SectionName = FName("Death3");
			break;
		case 3:
			SectionName = FName("Death4");
			break;
		default:
			break;
		}
		AnimInstance->Montage_JumpToSection(SectionName,OnDeathAnimMontage);
		
	}*/
	
}

bool ASBaseCharacter::CanMantle() const
{
	return !GetBaseCharacterMovementComponent()->IsFalling();
}

void ASBaseCharacter::UpdateStamina(float DeltaTime)
{
	if(FMath::IsNearlyZero(CurrentStamina))
	{
		GetBaseCharacterMovementComponent()->SetIsOutOfStamina(true);
	}

	if(CanRestoreStamina())
	{
		CurrentStamina += StaminaRestoreVelocity * DeltaTime;
		CurrentStamina = FMath::Clamp(CurrentStamina, 0.0f, MaxStamina);
	}

	if (FMath::IsNearlyEqual(CurrentStamina, MaxStamina))
	{
		GetBaseCharacterMovementComponent()->SetIsOutOfStamina(false);
	}
	//DebugDrawStamina(); Заменил на виджет, пока отключаю
}


bool ASBaseCharacter::CanRestoreStamina()
{
	return !SBaseCharacterMovementComponent->IsSprinting();
}

const FMantlingSettings& ASBaseCharacter::GetMantlingSettings(float LedgeGeight) const
{
	return LedgeGeight > LowMantleMaxHeight ? HighMantlingSettings : LowMantlingSettings;
}

void ASBaseCharacter::EnableRagdoll()
{
	GetMesh()->SetCollisionProfileName(CollisionProfileRagdoll);
	GetMesh()->SetSimulatePhysics(true);
}

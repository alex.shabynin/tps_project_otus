// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GenericTeamAgentInterface.h"
#include "TPSTypes.h"
#include "UObject/ScriptInterface.h"
#include "GameFramework/Character.h"
#include "SBaseCharacter.generated.h"

class ASPlayerController;
class USCharacterInventoryComponent;
class USInventoryItem;
DECLARE_MULTICAST_DELEGATE_OneParam(FOnAimingStateChanged, bool)
DECLARE_DELEGATE_OneParam(FOnInteractableObjectFound, FName);

class USActionComponent;
class USBaseCharacterMovementComponent;
class USCharacterAttributesComponent;
class ULedgeDetectorComponent;
class UCharacterEquipmentComponent;
class UAnimMontage;
class UCurveVector;
class UCurveFloat;
class IInteractable;
class ASEquippableItem;

USTRUCT(BlueprintType)
struct FMantlingSettings
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	UAnimMontage* MantlingMontage;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	UCurveVector* MantlingCurve;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, meta = (ClampMin = 0.0f,UIMin = 0.0f))
	float AnimationCorrectionXY = 65.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, meta = (ClampMin = 0.0f,UIMin = 0.0f))
	float AnimationCorrectionZ = 200.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, meta = (ClampMin = 0.0f,UIMin = 0.0f))
	float MaxHeight = 200.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, meta = (ClampMin = 0.0f,UIMin = 0.0f))
	float MinHeight = 100.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, meta = (ClampMin = 0.0f,UIMin = 0.0f))
	float MaxHeightStartTime = 0.0f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, meta = (ClampMin = 0.0f,UIMin = 0.0f))
	float MinHeightStartTime = 0.5f;
};

UCLASS(Abstract,NotBlueprintable)
class TPS_PROJECT_API ASBaseCharacter : public ACharacter, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type Reason) override;
	virtual void MoveForward(float Value) {};
	virtual void MoveRight(float Value) {};
	virtual void Turn(float Value) {};
	virtual void LookUp(float Value) {};
	virtual void TurnAtRate(float Value) {};
	virtual void LookUpAtRate(float Value) {};

	virtual void PossessedBy(AController* NewController) override;

	ASBaseCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void ChangeCrouchState();
	virtual void StartSprint();
	virtual void StopSprint();

	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	virtual void Mantle();
	void StartFire();
	void StopFire();
	void StartAim();
	void StopAim();
	void Reload();
	void NextItem();
	void PreviousItem();
	
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable, Category="Character")
	void OnStartAiming();
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable, Category="Character")
	void OnStopAiming();
	
	float GetAimingMovingSpeed() const;
	bool IsAiming() const;

	FOnAimingStateChanged OnAimingStateChanged;
	
	virtual bool CanJumpInternal_Implementation() const override;

	virtual void Falling() override;
	virtual void NotifyJumpApex() override;
	virtual void Landed(const FHitResult& Hit) override;

	UFUNCTION(BlueprintCallable)
	//float GetHealthPercent() const;

	/*
	 *Test stamina
	 */
	float GetStaminaPercent();
	UFUNCTION(BlueprintCallable)
	float GetStamina();

	FORCEINLINE USBaseCharacterMovementComponent* GetBaseCharacterMovementComponent() const { return SBaseCharacterMovementComponent; }

	UCharacterEquipmentComponent* GetCharacterEquipmentComponent_Mutable() const;
	
	const UCharacterEquipmentComponent* GetCharacterEquipmentComponent() const;

	USCharacterAttributesComponent* GetCharacterAttributesComponent() const;

	const USActionComponent* GetCharacterActionComponent() const;

	/**IGenericTeamAgentInterface:*/
	virtual FGenericTeamId GetGenericTeamId() const override;
	/**~IGenericTeamAgentInterface:*/

	UFUNCTION(BlueprintImplementableEvent)
	void OutOfStamina();

	void Interact();

	//void AddEquipmentItem(const TSubclassOf<ASEquippableItem> EquippableItemClass);

	bool PickupItem(TWeakObjectPtr<USInventoryItem> ItemToPickup);
	void UseInventory(ASPlayerController* PlayerController);

	void RestoreFullStamina();

	FOnInteractableObjectFound OnInteractableObjectFound;
	
protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	float MaxStamina = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	float StaminaRestoreVelocity = 25.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	float SprintStaminaConsumptionVelocity = 20.0f;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Player Controls")
	float BaseTurnRate = 45.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Controls")
	float BaseLookUpRate = 45.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Movement")
	float SprintMaxSpeed = 1000.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components")
	USCharacterAttributesComponent* CharacterAttributesComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components")
	ULedgeDetectorComponent* LedgeDetectorComponent;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category="Components")
	UCharacterEquipmentComponent* CharacterEquipmentComponent;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category="Components")
	USCharacterInventoryComponent* CharacterInventoryComponent;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category="Components")
	USActionComponent* CharacterActionComponent;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Player Movement | Mantling")
	FMantlingSettings HighMantlingSettings;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Player Movement | Mantling")
	FMantlingSettings LowMantlingSettings;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Player Movement | Mantling", meta = (ClampMin = 0.0f, UIMin = 0.0f))
	float LowMantleMaxHeight = 125.0f;
	
	virtual bool CanSprint();

	USBaseCharacterMovementComponent* SBaseCharacterMovementComponent;

	bool CanRestoreStamina();
	void DebugDrawStamina();

	virtual void OnDeath();

	bool CanMantle() const;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Character | Animations")
	UAnimMontage* OnDeathAnimMontage;

	//Damage from height in METERS!
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Character | Attributes")
	UCurveFloat* FallDamageCurve;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Character | Team")
	ETeams Team = ETeams::Enemy;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Character | Team")
	float LineOfSightDistance = 500.0f;

	void TraceLineOfSight();

	UPROPERTY()
	TScriptInterface<IInteractable> LineOfSightObject;
	
	virtual void OnStartAimingInternal();
	virtual void OnStopAimingInternal();

private:
	//float DefaultMaxMovSpeed = 0.0f;
	void TryChangeSprintState(float DeltaTime);
	
	bool bIsSprintRequested = false;
	//bool bIsSprinting = false;
	void UpdateStamina(float DeltaTime);

	float CurrentAimingMovingSpeed;
	bool bIsAiming;

	const FMantlingSettings& GetMantlingSettings(float LedgeGeight) const;

	//FTimerHandle DeathMontageTimer;
	void EnableRagdoll();
	
	FVector CurrentFallApex;
	
	float CurrentStamina = 0.0f;
};

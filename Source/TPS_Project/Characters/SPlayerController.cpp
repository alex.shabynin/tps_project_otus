// Fill out your copyright notice in the Description page of Project Settings.


#include "SPlayerController.h"

#include "CharacterEquipmentComponent.h"
#include "Components/InputComponent.h"
#include "SBaseCharacter.h"
#include "SPlayerHUDWidget.h"
#include "SReticleWidget.h"
#include "SAmmoWidget.h"
#include "Blueprint/UserWidget.h"

void ASPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	CachedBasecharacter = Cast<ASBaseCharacter>(InPawn);
	if(CachedBasecharacter.IsValid() && IsLocalController())
	{
		CreateAndInitializeWidgets();
		CachedBasecharacter->OnInteractableObjectFound.BindUObject(this, &ASPlayerController::OnInteractableObjectFound);
	}
}

void ASPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("Move Forward / Backward", this, &ASPlayerController::MoveForward);
	InputComponent->BindAxis("Move Right / Left", this, &ASPlayerController::MoveRight);
	InputComponent->BindAxis("Turn Right / Left Mouse", this, &ASPlayerController::Turn);
	InputComponent->BindAxis("Look Up / Down Mouse", this, &ASPlayerController::LookUp);
	InputComponent->BindAxis("TurnAtRate", this, &ASPlayerController::TurnAtRate);
	InputComponent->BindAxis("LookUpAtRate", this, &ASPlayerController::LookUpAtRate);
	InputComponent->BindAction("Mantle",IE_Pressed, this, &ASPlayerController::Mantle);
	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &ASPlayerController::Jump);
	InputComponent->BindAction("Crouch", EInputEvent::IE_Pressed, this, &ASPlayerController::ChangeCrouchState);
	InputComponent->BindAction("Run", EInputEvent::IE_Pressed, this, &ASPlayerController::StartSprint);
	InputComponent->BindAction("Run", EInputEvent::IE_Released, this, &ASPlayerController::StopSprint);
	InputComponent->BindAction("Fire",EInputEvent::IE_Pressed, this, &ASPlayerController::StartsFire);
	InputComponent->BindAction("Fire",EInputEvent::IE_Released, this, &ASPlayerController::StopFire);
	InputComponent->BindAction("Aim",EInputEvent::IE_Pressed, this, &ASPlayerController::StartAim);
	InputComponent->BindAction("Aim",EInputEvent::IE_Released, this, &ASPlayerController::StopAim);
	InputComponent->BindAction("Reload",EInputEvent::IE_Pressed, this, &ASPlayerController::Reload);
	InputComponent->BindAction("NextItem",EInputEvent::IE_Pressed, this, &ASPlayerController::NextItem);
	InputComponent->BindAction("PreviousItem",EInputEvent::IE_Pressed, this, &ASPlayerController::PreviousItem);
	InputComponent->BindAction(ActionInteract,EInputEvent::IE_Pressed, this, &ASPlayerController::Interact);
	InputComponent->BindAction("UseInventory",EInputEvent::IE_Pressed, this, &ASPlayerController::UseInventory);



}

void ASPlayerController::OnInteractableObjectFound(FName ActionName)
{
	if(!IsValid(PlayerHUDWidget))
	{
		return;
	}

	TArray<FInputActionKeyMapping> ActionKeys = PlayerInput->GetKeysForAction(ActionName);
	const bool HasAnyKeys = ActionKeys.Num() !=0;
	if(HasAnyKeys)
	{
		FName ActionKey = ActionKeys[0].Key.GetFName();
		PlayerHUDWidget->SetHighlightInteractableActionText(ActionKey);
	}
	PlayerHUDWidget->SetHighlightInteractableVisibility(HasAnyKeys);
}


void ASPlayerController::MoveForward(float Value)
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->MoveForward(Value);
	}
}

void ASPlayerController::MoveRight(float Value)
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->MoveRight(Value);
	}
}

void ASPlayerController::Turn(float Value)
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->Turn(Value);
	}
}

void ASPlayerController::LookUp(float Value)
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->LookUp(Value);
	}
}

void ASPlayerController::TurnAtRate(float Value)
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->TurnAtRate(Value);
	}
}

void ASPlayerController::LookUpAtRate(float Value)
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->LookUpAtRate(Value);
	}
}

void ASPlayerController::ChangeCrouchState()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->ChangeCrouchState();
	}
}

void ASPlayerController::Mantle()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->Mantle();
	}
}

void ASPlayerController::Jump()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->Jump();
	}
}

void ASPlayerController::StartSprint()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->StartSprint();
	}
}

void ASPlayerController::StopSprint()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->StopSprint();
	}
}

void ASPlayerController::StartsFire()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->StartFire();
	}
}
void ASPlayerController::StopFire()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->StopFire();
	}
}

void ASPlayerController::StartAim()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->StartAim();
	}
}

void ASPlayerController::StopAim()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->StopAim();
	}
}

void ASPlayerController::Reload()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->Reload();
	}
}

void ASPlayerController::NextItem()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->NextItem();
	}
}

void ASPlayerController::PreviousItem()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->PreviousItem();
	}
}

void ASPlayerController::Interact()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->Interact();
	}
}

void ASPlayerController::UseInventory()
{
	if (CachedBasecharacter.IsValid())
	{
		CachedBasecharacter->UseInventory(this);
	}

}

void ASPlayerController::CreateAndInitializeWidgets()
{
	if(!IsValid(PlayerHUDWidget))
	{
		PlayerHUDWidget = CreateWidget<USPlayerHUDWidget>(GetWorld(),PlayerHUDWidgetClass);
		if(IsValid(PlayerHUDWidget))
		{
			PlayerHUDWidget->AddToViewport();
		}
	}
	if(CachedBasecharacter.IsValid() && IsValid(PlayerHUDWidget))
	{
		USReticleWidget* ReticleWidget = PlayerHUDWidget->GetReticleWidget();
		if(IsValid(ReticleWidget))
		{
			CachedBasecharacter->OnAimingStateChanged.AddUFunction(ReticleWidget, FName("OnAimingStateChanged"));
		}
		USAmmoWidget* AmmoWidget = PlayerHUDWidget->GetAmmoWidget();
		if(IsValid(AmmoWidget))
		{
			UCharacterEquipmentComponent* CharacterEquipment = CachedBasecharacter->GetCharacterEquipmentComponent_Mutable();
			CharacterEquipment->OnCurrentWeaponAmmoChangedEvent.AddUFunction(AmmoWidget,FName("UpdateAmmoCount"));
		}
	}
}

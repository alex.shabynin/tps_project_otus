// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SPlayerController.generated.h"

class USPlayerHUDWidget;
/**
 * 
 */
UCLASS()
class TPS_PROJECT_API ASPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void SetPawn(APawn* InPawn);

protected:
	virtual void SetupInputComponent() override;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Widgets")
	TSubclassOf<USPlayerHUDWidget> PlayerHUDWidgetClass;

private:

	void OnInteractableObjectFound(FName ActionName);
	TSoftObjectPtr<class ASBaseCharacter> CachedBasecharacter;
	void CreateAndInitializeWidgets();
	USPlayerHUDWidget* PlayerHUDWidget = nullptr;
	
	void MoveForward(float Value);
	void MoveRight(float Value);
	void Turn(float Value);
	void LookUp(float Value);
	void TurnAtRate(float Value);
	void LookUpAtRate(float Value);
	void ChangeCrouchState();
	void Mantle();
	void Jump();
	void StartSprint();
	void StopSprint();
	void StartsFire();
	void StopFire();
	void StartAim();
	void StopAim();
	void Reload();
	void NextItem();
	void PreviousItem();
	//void EquipPrimaryWeapon();
	void Interact();
	void UseInventory();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPSTypes.h"
#include "Animation/AnimInstance.h"
#include "SBaseCharacterAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class TPS_PROJECT_API USBaseCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeBeginPlay() override;

	virtual void NativeUpdateAnimation(float DelataSeconds) override;

protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation")
	float Speed = 0.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation")
	bool bIsFalling = false;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation")
	bool bIsCrouching = false;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation")
	bool bIsSprinting = false;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation")
	bool bIsOutOfStamina = false;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation")
	bool bIsStrafing = false;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation", meta =(ClampMin= -180.0f,UIMin = -180.0f,ClampMax = 180.0f, UIMax = 180.0f))
	float Direction = 0.0f;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation")
	EEquippedItemType CurrentEquippedItem = EEquippedItemType::None;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation")
	FRotator AimRotation = FRotator::ZeroRotator;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation | Weapon")
	FTransform ForeGripSocketTransform;
	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Player animation | Weapon")
	bool bIsAiming;
	
private:
	TWeakObjectPtr<class ASBaseCharacter> CachedBaseCharacter;
};

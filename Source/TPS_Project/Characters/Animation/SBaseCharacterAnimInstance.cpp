// Fill out your copyright notice in the Description page of Project Settings.


#include "SBaseCharacterAnimInstance.h"
#include "SBaseCharacter.h"
//#include "GameFramework/CharacterMovementComponent.h"
#include "CharacterEquipmentComponent.h"
#include "SBaseCharacterMovementComponent.h"
#include "KismetAnimationLibrary.h"
#include "SRangeWeaponItem.h"

void USBaseCharacterAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
	checkf(TryGetPawnOwner()->IsA<ASBaseCharacter>(), TEXT("USBaseCharacterAnimInstance::NativeBeginPlay() USBaseCharacterAnimInstance can be used only with ASBaseCharacter"));
	CachedBaseCharacter = StaticCast<ASBaseCharacter*>(TryGetPawnOwner());
}

void USBaseCharacterAnimInstance::NativeUpdateAnimation(float DelataSeconds)
{
	if (!CachedBaseCharacter.IsValid())
	{
		return;
	}
	USBaseCharacterMovementComponent* CharacterMovement = CachedBaseCharacter->GetBaseCharacterMovementComponent();
	Speed = CharacterMovement->Velocity.Size();
	bIsFalling = CharacterMovement->IsFalling();
	bIsCrouching = CharacterMovement->IsCrouching();
	bIsSprinting = CharacterMovement->IsSprinting();
	bIsOutOfStamina = CharacterMovement->IsOutOfStamina();
	bIsAiming = CachedBaseCharacter->IsAiming();

	bIsStrafing = !CharacterMovement->bOrientRotationToMovement;
	Direction = UKismetAnimationLibrary::CalculateDirection(CharacterMovement->Velocity,CachedBaseCharacter->GetActorRotation());

	const UCharacterEquipmentComponent* CharacterEquipment = CachedBaseCharacter->GetCharacterEquipmentComponent();
	CurrentEquippedItem = CharacterEquipment->GetCurrentEquippedItemType();

	ASRangeWeaponItem* CurrentRangeWeapon = CharacterEquipment->GetCurrentRangeWeapon();
	if(IsValid(CurrentRangeWeapon))
	{
		ForeGripSocketTransform = CurrentRangeWeapon->GetForeGripTransform();
	}

	AimRotation = CachedBaseCharacter->GetBaseAimRotation();
}

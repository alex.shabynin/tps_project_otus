// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "SAnimNotify_EnableRagdoll.generated.h"

/**
 * 
 */
UCLASS()
class TPS_PROJECT_API USAnimNotify_EnableRagdoll : public UAnimNotify
{
	GENERATED_BODY()

public:
	
	//virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference) override;
};

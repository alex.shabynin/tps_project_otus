// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/Animation/AnimNotify_AttachEquippedItem.h"

#include "CharacterEquipmentComponent.h"
#include "SBaseCharacter.h"

void UAnimNotify_AttachEquippedItem::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
                                            const FAnimNotifyEventReference& EventReference)
{
	Super::Notify(MeshComp, Animation, EventReference);
	ASBaseCharacter* CharacterOwner = Cast<ASBaseCharacter> (MeshComp->GetOwner());
	if(!IsValid(CharacterOwner))
	{
		return;
	}
	CharacterOwner->GetCharacterEquipmentComponent_Mutable()->AttachCurrentItemToEquippedSocket();
}

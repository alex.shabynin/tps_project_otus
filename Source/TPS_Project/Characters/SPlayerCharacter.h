// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SBaseCharacter.h"
#include "SPlayerCharacter.generated.h"

/**
 * 
 */

class UCameraComponent;
class USpringArmComponent;

UCLASS(Blueprintable)
class TPS_PROJECT_API ASPlayerCharacter : public ASBaseCharacter
{
	GENERATED_BODY()
	
public:
	ASPlayerCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void MoveForward(float Value) override;
	virtual void MoveRight(float Value) override;
	virtual void Turn(float Value) override;
	virtual void LookUp(float Value) override;
	virtual void TurnAtRate(float Value) override;
	virtual void LookUpAtRate(float Value) override;

	virtual void OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;
	virtual void OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;

	virtual bool CanJumpInternal_Implementation() const override;
	virtual void OnJumped_Implementation() override;


protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	USpringArmComponent* SpringArmComp;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category = "Camera")
		UCameraComponent* CameraComp;
	virtual void OnStartAimingInternal() override;
	virtual void OnStopAimingInternal() override;
	
};

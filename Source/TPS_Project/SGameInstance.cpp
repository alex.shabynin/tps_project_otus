// Fill out your copyright notice in the Description page of Project Settings.


#include "SGameInstance.h"


bool USGameInstance::ProcessConsoleExec(const TCHAR* Cmd, FOutputDevice& Ar, UObject* Executor)
{
	bool bResult = Super::ProcessConsoleExec(Cmd, Ar, Executor);

	if(!bResult)
	{
		TArray<UGameInstanceSubsystem*> Subsystems = GetSubsystemArray<UGameInstanceSubsystem>();
		for(UGameInstanceSubsystem* SubSystem : Subsystems)
		{
			bResult |= SubSystem->ProcessConsoleExec(Cmd,Ar,Executor);
		}
	}

	return bResult;
}

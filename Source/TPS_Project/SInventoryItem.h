// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "UObject/NoExportTypes.h"
#include "SInventoryItem.generated.h"

class USInventoryItem;
class ASBaseCharacter;
class ASEquippableItem;
class ASPickable;

USTRUCT(BlueprintType)
struct FInventoryItemDescription : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Item View")
	FText Name;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Item View")
	UTexture2D* Icon;	
};

USTRUCT(BlueprintType)
struct FWeaponTableRow : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Weapon View")
	TSubclassOf<ASPickable> PickableActor;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Weapon View")
	TSubclassOf<ASEquippableItem> EquippableActor;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Weapon View")
	FInventoryItemDescription WeaponItemDescription;	
};

USTRUCT(BlueprintType)
struct FItemTableRow : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Item View")
	TSubclassOf<ASPickable> PickableActorClass;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Item View")
	TSubclassOf<USInventoryItem> InventoryItemClass;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Item View")
	FInventoryItemDescription InventoryItemDescription;
};

UCLASS(Blueprintable)
class TPS_PROJECT_API USInventoryItem : public UObject
{
	GENERATED_BODY()

public:
	void Initialize(FName DataTableID_In, const FInventoryItemDescription& Description_In);
	FName GetDataTableID() const;
	const FInventoryItemDescription& GetDescription() const;
	virtual bool IsEquippable() const;
	virtual bool IsConsumable() const;

	virtual bool Consume(ASBaseCharacter* ConsumeTarget) PURE_VIRTUAL(USInventoryItem::Consume, return false; );
	
protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Inventory Item")
	FName DataTableID = NAME_None;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Inventory Item")
	FInventoryItemDescription Description;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Inventory Item")
	bool bIsEquippable = false;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Inventory Item")
	bool bIsConsumable = false;

private:
	bool bIsInitialized = false;
};

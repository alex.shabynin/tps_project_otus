﻿#pragma once

#define ECC_Climbing ECC_GameTraceChannel1
#define ECC_Bullet ECC_GameTraceChannel2

const FName FXParamTraceEnd = FName("TraceEnd");
const FName SocketCharacterWeapon = FName("CharacterWeaponSocket");
const FName SocketWeaponGripHand = FName("GripHandSocket");
const FName SocketWeaponMuzzle = FName("MuzzleFlash");

const FName CollisionProfilePawn = FName("Pawn");
const FName CollisionProfileRagdoll = FName("Ragdoll");
const FName DebugCategoryLedgeDetection = FName("LedgeDetection");
const FName DebugCategoryCharacterAttributes = FName("CharacterAttributes");
const FName DebugCategoryRangeWeapon = FName("RangeWeapon");

const FName BB_CurrentTarget = FName("CurrentTarget");
const FName BB_NextLocation = FName("NextLocation");
const FName ActionInteract = FName("Interact");

//inline const TCHAR* DataTableWeapons = TEXT("/Game/ThirdPersonShooter/Data/DataTables/DT_WeaponList.DT_WeaponList");

UENUM(BlueprintType)
enum class EEquippedItemType : uint8
{
	None,
	Pistol,
	Rifle
	//MAX UMETA(Hidden)

};
UENUM(BlueprintType)
enum class EAmmunitionType : uint8
{
	None,
	Pistol,
	Rifle,
	MAX UMETA(Hidden)
};
UENUM(BlueprintType)
enum class EEquipmentSlots : uint8
{
	None,
	SideArm,
	PrimaryWeapon,
	PrimaryItemSlot,
	MAX UMETA(Hidden)
};
UENUM(BlueprintType)
enum class ETeams : uint8
{
	Player,
	Enemy
};
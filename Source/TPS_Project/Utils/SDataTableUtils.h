// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SInventoryItem.h"
#include "Engine/DataTable.h"

/**
 * 
 */
namespace SDataTableUtils
{
 FWeaponTableRow* FindWeaponData(const FName WeaponID);
 FItemTableRow* FindInventoryItemData(const FName ItemID);
};

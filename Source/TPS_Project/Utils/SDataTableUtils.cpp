// Fill out your copyright notice in the Description page of Project Settings.


#include "Utils/SDataTableUtils.h"
#include "Engine/DataTable.h"
#include "SInventoryItem.h"
#include "TPSTypes.h"

FWeaponTableRow* SDataTableUtils::FindWeaponData(const FName WeaponID)
{
	static const FString ContextString(TEXT("FindWeaponData"));
	UDataTable* WeaponDataTable = LoadObject<UDataTable>(nullptr,TEXT("/Game/ThirdPersonShooter/Data/DataTables/DT_WeaponList.DT_WeaponList"));
	if(WeaponDataTable == nullptr)
	{
		return nullptr;
	}
	return WeaponDataTable->FindRow<FWeaponTableRow>(WeaponID, ContextString);
}

FItemTableRow* SDataTableUtils::FindInventoryItemData(const FName ItemID)
{
	static const FString ContextString(TEXT("FindInventoryItemData"));
	UDataTable* InventoryItemDataTable = LoadObject<UDataTable>(nullptr,TEXT("/Game/ThirdPersonShooter/Data/DataTables/DT_InventoryItemList.DT_InventoryItemList"));
	if(InventoryItemDataTable == nullptr)
	{
		return nullptr;
	}
	return InventoryItemDataTable->FindRow<FItemTableRow>(ItemID, ContextString);
}
